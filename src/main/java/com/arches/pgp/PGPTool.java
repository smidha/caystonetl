package com.arches.pgp;

import java.util.ResourceBundle;

import com.arches.caystonETL.UserResponseApp;

public class PGPTool {

	public PGPTool(){
System.out.println("PGPTool started..");
	}
	public  boolean  testDecrypt(String dInput,String dOutput,String passphrase,String key) throws Exception {
		PGPFileProcessor p = new PGPFileProcessor();
		p.setInputFileName(dInput);
		p.setOutputFileName(dOutput);
		p.setPassphrase(passphrase);
		p.setSecretKeyFileName(key);
		boolean result= p.decrypt();
		if(result==false)
		{
			UserResponseApp.errorLogs.add("False result in decryption for "+dInput);
		}
		return result;
	}

	public boolean testEncrypt(String eInput, String eOutput, String passphrase,String key) throws Exception {
		PGPFileProcessor p = new PGPFileProcessor();
		p.setInputFileName(eInput);
		p.setOutputFileName(eOutput);
		//p.setPassphrase(passphrase);
		p.setPublicKeyFileName(key);
		boolean result=p.encrypt();
		if(result==false)
		{
			UserResponseApp.errorLogs.add("False result in encryption for "+eInput);
		}
		return result;
	}
	

}
