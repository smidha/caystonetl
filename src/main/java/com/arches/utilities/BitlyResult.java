package com.arches.utilities;

public class BitlyResult {
	public String hash;
	public String shortCNAMEUrl;
	public String shortKeywordUrl;
	public String shortUrl;
	public String userHash;
	public BitlyResult(String hash, String shortCNAMEUrl, String shortKeywordUrl, String shortUrl, String userHash) {
		super();
		this.hash = hash;
		this.shortCNAMEUrl = shortCNAMEUrl;
		this.shortKeywordUrl = shortKeywordUrl;
		this.shortUrl = shortUrl;
		this.userHash = userHash;
	}
}
