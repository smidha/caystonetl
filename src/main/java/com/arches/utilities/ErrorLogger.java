package com.arches.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;

import com.arches.caystonETL.UserResponseApp;


public class ErrorLogger {

	String pathname;
	public ErrorLogger()
	{
		ResourceBundle resource=ResourceBundle.getBundle(com.arches.caystonETL.UserResponseApp.configPropertiesLocation);
		this.pathname=resource.getString("errorLogsLocation")+"ARCHES_GILEAD_CAYSTON_CAP_ENROLLMENT_"+DateTime.getDateTime()+"_"+UserResponseApp.recordsCountsForSummary+"_ERROR.txt";
		
	}
	public void writeErrorLogs(List<String> errors) throws IOException
	{ 
		errors.add(0,Calendar.getInstance(TimeZone.getTimeZone("EST")).getTime().toString());
		
		File file=new File(pathname);
		//true for append mode
		FileUtils.writeLines(file,errors,true);
    
	}
}
