package com.arches.utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;

import com.arches.caystonETL.CaystonClient;
import com.arches.caystonETL.EmailGenerator;
import com.arches.caystonETL.ProcessFiles;
import com.arches.caystonETL.UserResponseApp;


public class SummaryReportGenerator{

	String pathname;
	public  SummaryReportGenerator()
	{
		// TODO Auto-generated constructor stub
	try{
		ResourceBundle resource=ResourceBundle.getBundle(UserResponseApp.configPropertiesLocation);
		this.pathname=resource.getString("summaryReportLocation")+"ARCHES_GILEAD_CAYSTON_CAP_ENROLLMENT_"+DateTime.getDateTime()+"_"+UserResponseApp.recordsCountsForSummary+"_SUMMARY"+".txt";
		}
	catch(Exception e)
	{
		UserResponseApp.errorLogs.add("Error in locating config file or a property for summary report generator."+e);
        UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);

	}
	}
	public void generateReport(long total,long successfulRegistration,long successfulReprofiling, long validationErrors, long duplicateEnrollmentError,long UUIDErrorForReprofiling,long reprofilingError,long registrationError,long capIDError) throws IOException
	{
		long totalSuccessful=successfulRegistration+successfulReprofiling;
		List<String> report=new ArrayList<String>();
		report.add(Calendar.getInstance().getTime().toString());
		report.add("Files scanned: "+UserResponseApp.stringOfFileNamesForSummary);
		report.add("Total number of users:"+total);
		report.add("Number of users successfully registered or reprofiled:"+totalSuccessful);
		report.add("Number of users successfully registered:"+successfulRegistration);
		report.add("Number of users successfully reprofiled:"+successfulReprofiling);
		report.add("Number of users with validation errors:"+validationErrors);
		report.add("Number of users with duplicate enrollment error:"+duplicateEnrollmentError);
		report.add("Number of users with invalid UUID for reprofiling:"+UUIDErrorForReprofiling);
		report.add("Number of users with error in registration:"+registrationError);
		report.add("Number of users with error in reprofiling:"+reprofilingError);
		report.add("Number of users with CAP ID error in reprofiling:"+capIDError);
		File file=new File(pathname);
		FileUtils.writeLines(file,report,true);
    
	}
	
}
