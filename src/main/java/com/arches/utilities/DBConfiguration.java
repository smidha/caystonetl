package com.arches.utilities;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.arches.caystonETL.UserResponseApp;

	public class DBConfiguration {
		/*
		 * To change this license header, choose License Headers in Project Properties.
		 * To change this template file, choose Tools | Templates
		 * and open the template in the editor.
		 */

		    //declare class variables
		    public static String driver;
		    public static String url;
		    public static String username;
		    public static String password;
		    public static String DB;
		    public static String table;
		    public static String DBfile;
		    public static String tenantuid;
		    public static String roles;
	        public static int isActive;
	        public static int isDeleted;
	        public static String questionsTable;
	        public static String questionsResponseTable;
	        public static String digestorAlgorithm;
	        public static String encryptionPassword;
	        public static String encryptionAlgorithm;
		       
		    
		        public static void configureDB(String DBf)
		        {
		            //initialize variables and get name of DB Credentials file
		            DBfile=DBf;
		        try{
		            ResourceBundle DBResource = ResourceBundle.getBundle(DBfile);
		            url=DBResource.getString("url");
		            username=DBResource.getString("username");
		            password=DBResource.getString("password");
		            DB=DBResource.getString("DB");
		            table=DBResource.getString("table");
		            questionsTable=DBResource.getString("questionsTable");
		            questionsResponseTable=DBResource.getString("questionsResponseTable");
		            driver=DBResource.getString("driver");
		            tenantuid=DBResource.getString("tenantuid");
		            roles=DBResource.getString("roles");
		            isActive=Integer.parseInt(DBResource.getString("isActive"));
		            isDeleted=Integer.parseInt(DBResource.getString("isDeleted"));
		            encryptionAlgorithm=DBResource.getString("encryptionAlgorithm");
		            digestorAlgorithm=DBResource.getString("digestorAlgorithm");
		            encryptionPassword=DBResource.getString("encryptionPassword");
		        }
		         catch(Exception e)
		            {
		                System.out.println("Error in configuring DB credentials."+e);
		                UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
		            }
		        }
		}
		    





