package com.arches.utilities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 *
 * Program to generate UUID and return it in String format
 */
public class UUIDGenerator implements IdentifierGenerator {
    private String uuid;
 
    public static String getUUID()
    {
        UUID id=UUID.randomUUID();
        return id.toString();
    }
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
		// TODO Auto-generated method stub
			return getUUID();
	}
    
}
