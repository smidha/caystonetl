package com.arches.caystonETL;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.jdbc.Work;

import com.arches.model.RefillHistory;
import com.arches.model.UserDet;
import com.arches.model.UserProfile;
import com.arches.utilities.DateTime;
import com.arches.utilities.UUIDGenerator;
import com.google.gson.Gson;
import com.jcraft.jsch.UserAuthGSSAPIWithMIC;

public class CaystonClientOriginal {

	String pathToHibernateConfig=UserResponseApp.hibernateConfig;
	static long successfulRegistration=0;
	static long successfulReprofiling=0;
	static long validationErrorRecords=0;
	static long duplicateEnrollmentError=0;
	static long UUIDErrorForReprofiling=0;
	static long errorInReprofilingRecords=0;
	static long errorInRegistrationRecords=0;
	static long CAPIDErrorForReprofiling=0;
	
	public CaystonClientOriginal()
	{
		
	}
	public String registerUsers(UserDet user, UserProfile userProfile)
	{
		System.out.println("in cayston client : register user"+user.getUserType());
		//format dob because DB format us yyyy-MM-dd but coding format and text file format is yyyyMMdd
		if(userProfile.getDob()!=null && userProfile.getDob().length()!=0)
		{
			String formattedDate=DateTime.changeFormat(userProfile.getDob(),"yyyyMMdd","yyyy-MM-dd");
			userProfile.setDob(formattedDate);
			if(formattedDate==null)
			{
			UserResponseApp.errorLogs.add("[CaystonClient.registerUsers]:Error/parse exception in formatting DOB format. DateTime transformation utility returned null");
			}
			System.out.println("Formatted DOB for DB storage:"+userProfile.getDob());
		}
		String response=null;
		
		//update user
		if(user.getUserType()==2)
		{
			//reprofiling code
			response=registerUpdateUser(user, userProfile);
			if(response.equals("error"))
			{
				System.out.println("error response..error in reprofiling");
				
				UserResponseApp.errorLogs.add("'Error' response from reprofiling method for user:"+new Gson().toJson(user));
				errorInReprofilingRecords++;
			
			}
			else if(response.equals("success"))
			{
				System.out.println("successfull reprofiling..");
				successfulReprofiling++;
				
			}
			else if(response.equals("UUIDError"))
			{
				System.out.println("UUID not found:"+user.getUuid());
				UserResponseApp.errorLogs.add("UUID not found in database. Cannot reprofile user:"+new Gson().toJson(user));;
				UUIDErrorForReprofiling++;
			}
			else if(response.equals("CAPIDError"))
			{
				System.out.println("UUID not found:"+user.getUuid());
				UserResponseApp.errorLogs.add("CAP ID not found in database. Cannot reprofile user:"+new Gson().toJson(user));;
				CAPIDErrorForReprofiling++;
			}
			else 
			{
				System.out.println("unknown value returned by reprofiling method");
				UserResponseApp.errorLogs.add("Cannot recognize value/result returned by reprofiling method");
				errorInReprofilingRecords++;
			}
			
		}
		
		//legacy or new user
		else if(user.getUserType().intValue()==0 || user.getUserType().intValue()==1)
		{
			System.out.println("legacy or new: register user");
			 
			BigDecimal bd_createdOn=new BigDecimal(System.currentTimeMillis());
			user.setCreatedOn(bd_createdOn);
			userProfile.setCreatedOn(bd_createdOn);
			//call registration method
			response=registerLegacyOrNewUser(user, userProfile);
			
			//"duplicate","success","error" handled in registerlegacyornewuser itself
			if(response==null)
			{
				System.out.println("null response received from registerLecayOrNewUser for user:"+new Gson().toJson(user));
				UserResponseApp.errorLogs.add("null response received from registerLecayOrNewUser for user:"+new Gson().toJson(user));
				errorInRegistrationRecords++;
			}
		}
		return response;
	}
	public String registerLegacyOrNewUser(UserDet user, UserProfile userProfile)
	{
		System.out.println("in register legacy or new user");
		String result=null;
		
		try{
			//check for duplicate enrollment
			System.out.println("call duplication checking method");
			//call duplication checking method
		    result=checkDuplicateEnrollment(user, userProfile);
		    //analyse response to duplication check
		    
		    //duplicate: do not register
		    if(result.equals("duplicate"))
		    {
		    	duplicateEnrollmentError++;
		    	System.out.println(user.getEmail()+" already exists");
		    	UserResponseApp.errorLogs.add("Duplicate enrollment. User already present in DB. User:"+new Gson().toJson(user));
		    	//immediately return 'duplicate'. Do not proceed ahead with registration
		    	return result;
		    }
			
		    //start registration
			
			Configuration cfg=new Configuration();
			cfg.configure(this.pathToHibernateConfig);
			SessionFactory sessionFactory=cfg.buildSessionFactory();
			Session session=sessionFactory.openSession();
		
			/*session.doWork(new Work() {
			
			public void execute(Connection connection) throws SQLException {
				// TODO Auto-generated method stub
				connection.createStatement().execute("SET IDENTITY_INSERT user_det ON");
			}
			});*/
			
			//Register user in db
			Serializable IdForUser=session.save(user);
		
			//DB auto generated ID for userDet is UserId for userProfile
			System.out.println("adding first shipment date for new/legacy user to refill history");
			Serializable refillHistory_firstShipment=session.save(refillHistoryFirstShipDateNewLegacyUser(user, userProfile));
					RefillHistory[] refillHistoryPatternArray=refillHistoryRefillPatternNewLegacyUser(user, userProfile);
			System.out.println("adding cap pattern for new/legacy user to refill history");

			if(refillHistoryPatternArray!=null)
			{
				for(int ctr=0;ctr<refillHistoryPatternArray.length;ctr++)
				{
					Serializable refillHistoryPattern=session.save(refillHistoryPatternArray[ctr]);
				}
			}	
			System.out.println("adding recent shipment date for new/legacy user to refill history");

			Serializable refillHistory_mostRecent=session.save(refillHistoryRecentShipDateNewLegacyUser(user, userProfile));

			//save profile object
			userProfile.setUserId(IdForUser.toString());
			Serializable IdForProfile=session.save(userProfile);
			session.beginTransaction().commit();
			session.clear();
			session.close();
			System.out.println("Primary key(ID) returned:"+IdForUser.toString());
			result="success";
			successfulRegistration++;
			//new or legacy user registered. send welcome email
			if(user.getEmail()!=null && user.getEmail().length()>0)
			{
			EmailGenerator welcomeEmailGenerator=new EmailGenerator(UserResponseApp.mandrillFile);
			welcomeEmailGenerator.generateWelcomeEmail(user);
			}
		}
			
		catch(Exception e)
		{
			UserResponseApp.errorLogs.add("Error in adding user to DB. Exception:"+e);
			e.printStackTrace();
			result="error";
			errorInRegistrationRecords++;
		}
		finally
		{
			return result;
		}
		
	}
	
	public String checkDuplicateEnrollment(UserDet user,UserProfile userProfile)
	{
		String result="NotDuplicate";
		Session session=null;
		try{
		
			Configuration cfg=new Configuration();
		cfg.configure(this.pathToHibernateConfig);
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		session=sessionFactory.openSession();
		System.out.println("user's email is:"+user.getEmail());
		
		if(user.getEmail()==null || user.getEmail().equals(""))
		{
			//text file record without email cannot be compared. Has to be registered. Hence return 'not duplicate'
			String hql_capID = "SELECT capPatientId FROM UserDet where capPatientId=:capIDParam";
			Query query_capID = session.createQuery(hql_capID);
			query_capID.setParameter("capIDParam",user.getCapPatientId());
			List resultsbasedOnCapID = query_capID.list();
			
			if(resultsbasedOnCapID!=null && resultsbasedOnCapID.size()!=0)
			{
				//cap id found in db..return 'duplicate' instantly
				result="duplicate";
			
				
			}
			else
			{
				//Empty email,cap id not found.Hence not duplicate 
				result="NotDuplicate";
			
			}
				
			return result;

		}
		//If code is at this point=> email is not null=>check duplication on basis of email
		
		// text fiel record has an email.Check for duplication in db
		String hql = "SELECT email FROM UserProfile where email=:emailParam";
		Query query = session.createQuery(hql);
		query.setParameter("emailParam",user.getEmail());
		System.out.println("after setting query param");
		List results = query.list();
		System.out.println("results.."+results);
		if(results.size()!=0)
		{
			//email record found in db. set result to  'duplicate'
			result="duplicate";
		}
		System.out.println("returning duplication result:"+result);
		
		}
		catch(Exception e)
		{
			System.out.println("Exception in duplication checking.."+e);
			
		}
		finally{
			if(session!=null)
			{
			session.clear();
			session.close();
			}
			//return result value
		return result;
			}
	}
	
	
	
	
	public String registerUpdateUser(UserDet user,UserProfile userProfile)
	{
		try{
			if(user.getUuid()==null || user.getUuid().length()==0)
			{
				String resultForNoUUIDCase=registerUpdateUsersWithoutUUID(user,userProfile);
				return resultForNoUUIDCase;
			}
			
			
						Configuration cfg=new Configuration();
						cfg.configure(this.pathToHibernateConfig);
						SessionFactory sessionFactory=cfg.buildSessionFactory();
						Session session=sessionFactory.openSession();
						String hql = "FROM UserDet where uuid=:uuidParam";
						Query query = session.createQuery(hql);
						query.setParameter("uuidParam",user.getUuid());
						List results = query.list();
						if(results==null || results.size()==0)
						{
								return "UUIDError";
						}
						UserDet userDetFromDB=(UserDet) results.get(0);
						//update the temp obj from db and send it back to db
						userDetFromDB.setFirstName(user.getFirstName());
						userDetFromDB.setLastName(user.getLastName());
						userDetFromDB.setEmail(user.getEmail());
						userDetFromDB.setAddress1(user.getAddress1());
						userDetFromDB.setAddress2(user.getAddress2());
						userDetFromDB.setState(user.getState());
						userDetFromDB.setCity(user.getCity());
						userDetFromDB.setZip(user.getZip());
					//uuid stays same
						userDetFromDB.setUserType(user.getUserType());
						//relationship column exists in user profile
						userDetFromDB.setCapPatientId(user.getCapPatientId());
						userDetFromDB.setCapEnrollmentDate(user.getCapEnrollmentDate());
						//first shipment date, recent shipment date,cap refill pattern are in profile
						userDetFromDB.setPharmacy(user.getPharmacy());
						
						String pharmacyInfoStr=getPharmacyInfo(user.getPharmacy());
						if(pharmacyInfoStr!=null  && pharmacyInfoStr.length()>0)
						{	
						String pharmacyInfo[]=pharmacyInfoStr.split(",");
						if(pharmacyInfo.length==3)
						{	
						userDetFromDB.setPharmacyName(pharmacyInfo[0]);
						userDetFromDB.setPharmacyContactNumber(pharmacyInfo[1]);
						userDetFromDB.setPharmacy_id(Integer.parseInt(pharmacyInfo[2]));
						userProfile.setPharmacy(Integer.parseInt(pharmacyInfo[2]));
						}
						}
					
						userDetFromDB.setPapPatientStatus(user.getPapPatientStatus());
						//pap first dispense date,dispense activity,latest dispense date in profile
						userDetFromDB.setCapHasInsurance(user.getCapHasInsurance());
						userDetFromDB.setCapPatientStatus(user.getCapPatientStatus());
						userDetFromDB.setCapFormId(user.getCapFormId());
						userDetFromDB.setBrandedOptCheck(user.getBrandedOptCheck());
						userDetFromDB.setRemainingRefillCount(user.getRemainingRefillCount());
						userDetFromDB.setOptInDate(user.getOptInDate());
						userDetFromDB.setOptOutDate(user.getOptOutDate());
						
						//update id/creation time will already be there in db obj
						
						userProfile.setCreatedOn(userDetFromDB.getCreatedOn());
						//userdet from db retains its uuid. But userProfile adds a new object/row. So it need to copy uuid from user det
						userProfile.setUserUid(userDetFromDB.getUuid());
						//set modified field for user obj since its being modified- user from db might have null or earlier modification date
						
						BigDecimal bd_modifiedOn=new BigDecimal(System.currentTimeMillis());
							
						userDetFromDB.setModifiedOn(bd_modifiedOn);
						userProfile.setModifiedOn(bd_modifiedOn);
						System.out.println("Sending user obj for MERGING");
						session.merge(userDetFromDB);
						System.out.println("user merged");
						
						//merge/update user obj
						System.out.println("Sending user obj for MERGING based on cap id");
						session.merge(userDetFromDB);
						System.out.println("user merged");
							
						RefillHistory firstShipDate=refillHistoryFirstShipDateUpdateUser(userDetFromDB, userProfile);
						
						
						RefillHistory[] refillPatternArray=refillHistoryRefillPatternUpdateUser(userDetFromDB, userProfile);
						

						RefillHistory refillHistoryrecentShipDate=refillHistoryRecentShipDateUpdateUser(userDetFromDB, userProfile);
						
						//update shipment date
						if(firstShipDate!=null)
						{
							System.out.println("updating first shipment date for update user in refill history with:"+firstShipDate.getRefilled_on());
							String updateRefillHQL1="update RefillHistory set refilled_on=:refilledOnParam,modifiedOn=:modifiedOnParam where id=:idParam";
							
							Query updateRefillQuery1=session.createQuery(updateRefillHQL1);
							
							updateRefillQuery1.setParameter("refilledOnParam",firstShipDate.getRefilled_on());
							updateRefillQuery1.setParameter("idParam",firstShipDate.getId());
							updateRefillQuery1.setParameter("modifiedOnParam",firstShipDate.getModifiedOn());
						
							int result=updateRefillQuery1.executeUpdate();
							System.out.println("result for update user updating first shipment date:"+result);
							
							
										}
					
						
				
						//update refill pattern
						if(refillPatternArray!=null)
						{
							for(int ctr=0;ctr<refillPatternArray.length;ctr++)
							{
								System.out.println("updating cap pattern for update user in refill history with:"+refillPatternArray[ctr].getRefilled_on());
								
								System.out.println(refillPatternArray[ctr].getRefilled_on());
								if(refillPatternArray[ctr]==null)
								{
									continue;
								}
								if(refillPatternArray[ctr].getId()==null)
								{Serializable refillHistory_capPattern=session.save(refillPatternArray[ctr]);}
								else
								{
									session.update(refillPatternArray[ctr]);
								}
							}
						}
						if(refillHistoryrecentShipDate!=null)
						{
							System.out.println("updating recent shipment date for update user in refill history :"+refillHistoryrecentShipDate.getRefilled_on());
							if(refillHistoryrecentShipDate.getId()!=null)
							{
							String updateRefillHQL="update RefillHistory set refilled_on=:refilledOnParam,modifiedOn=:modifiedOnParam where id=:idParam";
							Query updateRefillQuery=session.createQuery(updateRefillHQL);
							updateRefillQuery.setParameter("refilledOnParam",refillHistoryrecentShipDate.getRefilled_on());
							updateRefillQuery.setParameter("idParam",refillHistoryrecentShipDate.getId());
							updateRefillQuery.setParameter("modifiedOnParam",firstShipDate.getModifiedOn());
							
							int result2=updateRefillQuery.executeUpdate();
							}
							else
							{
								
								refillHistoryrecentShipDate.setModifiedOn(null);
								session.save(refillHistoryrecentShipDate);
								
							}
						}

						
						//add a row for userProfile in user profile table
						//user profile from txt file will not be having user_id(PK in user table for this obj).so set it
					
						userProfile.setUserId(userDetFromDB.getId().toString());
						
						Serializable newUserProfileID=session.save(userProfile);
						
						session.beginTransaction().commit();
						if(session!=null)
						{
							session.clear();
							session.close();
						}
						System.out.println("session closed. new user profile id for update user:"+newUserProfileID.toString());
						return "success";
		
		
			}
		catch(Exception e)
		{
			System.out.println("Error in reprofiling user."+e);
			UserResponseApp.errorLogs.add("Error in reprofiling method."+e);
			e.printStackTrace();
			return "error";
		}
	}
	
	public  String registerUpdateUsersWithoutUUID(UserDet user,UserProfile userProfile)
	{	
		System.out.println("registering update user without UUID..");
		Configuration cfg=new Configuration();
		cfg.configure(this.pathToHibernateConfig);
		SessionFactory sessionFactory1=cfg.buildSessionFactory();
		Session session1=sessionFactory1.openSession();
		String hql = "FROM UserDet where capPatientId=:capIDParam";
		Query query = session1.createQuery(hql);
		query.setParameter("capIDParam",user.getCapPatientId());
		List results = query.list();
		if(session1!=null)
		{
			session1.flush();
			session1.clear();
			session1.close();
		}
		if(results==null || results.size()==0)
		{
			System.out.println("Cap id not found in DB");
			return "CAPIDError";
		}
		else
		{
			System.out.println("Cap id found in DB..register. list size:"+results.size());
			//reprofile user
			UserDet userDetFromDB=(UserDet) results.get(0);
			
			//update the temp obj from db and send it back to db
			userDetFromDB.setFirstName(user.getFirstName());
			userDetFromDB.setLastName(user.getLastName());
			userDetFromDB.setEmail(user.getEmail());
			userDetFromDB.setAddress1(user.getAddress1());
			userDetFromDB.setAddress2(user.getAddress2());
			userDetFromDB.setState(user.getState());
			userDetFromDB.setCity(user.getCity());
			userDetFromDB.setZip(user.getZip());
		
		//uuid stays same
			userDetFromDB.setUserType(user.getUserType());
			//relationship column exists in user profile
			userDetFromDB.setCapPatientId(user.getCapPatientId());
			userDetFromDB.setCapEnrollmentDate(user.getCapEnrollmentDate());
			//first shipment date, recent shipment date,cap refill pattern are in profile
			userDetFromDB.setPharmacy(user.getPharmacy());
			
			String pharmacyInfoStr=getPharmacyInfo(user.getPharmacy());
			if(pharmacyInfoStr!=null  && pharmacyInfoStr.length()>0)
			{	
			String pharmacyInfo[]=pharmacyInfoStr.split(",");
			if(pharmacyInfo.length==3)
			{	
			userDetFromDB.setPharmacyName(pharmacyInfo[0]);
			userDetFromDB.setPharmacyContactNumber(pharmacyInfo[1]);
			userDetFromDB.setPharmacy_id(Integer.parseInt(pharmacyInfo[2]));
			userProfile.setPharmacy(Integer.parseInt(pharmacyInfo[2]));
			}
			}
			userDetFromDB.setPapPatientStatus(user.getPapPatientStatus());
			
			//pap first dispense date,dispense activity,latest dispense date in profile
			userDetFromDB.setCapHasInsurance(user.getCapHasInsurance());
			userDetFromDB.setCapPatientStatus(user.getCapPatientStatus());
			userDetFromDB.setCapFormId(user.getCapFormId());
			userDetFromDB.setBrandedOptCheck(user.getBrandedOptCheck());
			userDetFromDB.setRemainingRefillCount(user.getRemainingRefillCount());
			userDetFromDB.setOptInDate(user.getOptInDate());
			userDetFromDB.setOptOutDate(user.getOptOutDate());
			
			//update id/creation time will already be there in db obj
		
			userProfile.setCreatedOn(userDetFromDB.getCreatedOn());
			//set modified field for user obj since its being modified- user from db might have null or earlier modification date
			
			BigDecimal bd_modifiedOn=new BigDecimal(System.currentTimeMillis());
				
			userDetFromDB.setModifiedOn(bd_modifiedOn);
			userProfile.setModifiedOn(bd_modifiedOn);
			
			SessionFactory sessionFactory2=cfg.buildSessionFactory();
			Session session2=sessionFactory2.openSession();
			
			//merge/update user obj
			System.out.println("Sending user obj for MERGING based on cap id");
			session2.merge(userDetFromDB);
			System.out.println("user merged");
				
			RefillHistory firstShipDate=refillHistoryFirstShipDateUpdateUser(userDetFromDB, userProfile);
		
		
			RefillHistory[] refillPatternArray=refillHistoryRefillPatternUpdateUser(userDetFromDB, userProfile);
			

			RefillHistory refillHistoryrecentShipDate=refillHistoryRecentShipDateUpdateUser(userDetFromDB, userProfile);
			
			//update shipment date
			if(firstShipDate!=null)
			{
				System.out.println("updating first shipment date for update user in refill history with:"+firstShipDate.getRefilled_on());
				String updateRefillHQL1="update RefillHistory set refilled_on=:refilledOnParam,modifiedOn=:modifiedOnParam where id=:idParam";
				
				Query updateRefillQuery1=session2.createQuery(updateRefillHQL1);
				
				updateRefillQuery1.setParameter("refilledOnParam",firstShipDate.getRefilled_on());
				updateRefillQuery1.setParameter("idParam",firstShipDate.getId());
				updateRefillQuery1.setParameter("modifiedOnParam",firstShipDate.getModifiedOn());
			
				int result=updateRefillQuery1.executeUpdate();
				System.out.println("result for update user updating first shipment date:"+result);
				
				
							}
		
			
	
			//update refill pattern
			if(refillPatternArray!=null)
			{
				for(int ctr=0;ctr<refillPatternArray.length;ctr++)
				{
					System.out.println("updating cap pattern for update user in refill history with:"+refillPatternArray[ctr].getRefilled_on());
					
					System.out.println(refillPatternArray[ctr].getRefilled_on());
					if(refillPatternArray[ctr]==null)
					{
						continue;
					}
					if(refillPatternArray[ctr].getId()==null)
					{Serializable refillHistory_capPattern=session2.save(refillPatternArray[ctr]);}
					else
					{
						session2.update(refillPatternArray[ctr]);
					}
				}
			}
			if(refillHistoryrecentShipDate!=null)
			{
				System.out.println("updating recent shipment date for update user in refill history :"+refillHistoryrecentShipDate.getRefilled_on());
				if(refillHistoryrecentShipDate.getId()!=null)
				{
				String updateRefillHQL="update RefillHistory set refilled_on=:refilledOnParam,modifiedOn=:modifiedOnParam where id=:idParam";
				Query updateRefillQuery=session2.createQuery(updateRefillHQL);
				updateRefillQuery.setParameter("refilledOnParam",refillHistoryrecentShipDate.getRefilled_on());
				updateRefillQuery.setParameter("idParam",refillHistoryrecentShipDate.getId());
				updateRefillQuery.setParameter("modifiedOnParam",firstShipDate.getModifiedOn());
				
				int result2=updateRefillQuery.executeUpdate();
				}
				else
				{
					
					refillHistoryrecentShipDate.setModifiedOn(null);
					session2.save(refillHistoryrecentShipDate);
					
				}
			}

			//add a row for userProfile in user profile table
			//user profile from txt file will not be having user_id(PK in user table for this obj).so set it
			userProfile.setUserId(userDetFromDB.getId().toString());
			Serializable newUserProfileID=session2.save(userProfile);
			session2.beginTransaction().commit();
			session2.flush();
			session2.clear();
			session2.close();
			System.out.println("session closed. new user profile id:"+newUserProfileID.toString());
			return "success";

		}
		
	}
	public static RefillHistory refillHistoryFirstShipDateNewLegacyUser(UserDet user,UserProfile userProfile)
	{

		RefillHistory firstShipDate=new RefillHistory();
		firstShipDate.setUser_uid(user.getUuid());
	
		if(userProfile.getSpFirstShipDate()!=null && !userProfile.getSpFirstShipDate().equals(""))
		{
			firstShipDate.setRefilled_on(DateTime.changeFormat(userProfile.getSpFirstShipDate(),"yyyyMMdd","yyyy-MM-dd"));
		}
		else
		{
			firstShipDate.setRefilled_on(null);
		}
			firstShipDate.setPharmacy_uid(user.getPharmacy_id());
		firstShipDate.setUuid(UUIDGenerator.getUUID());
		firstShipDate.setIsActive(user.getIsActive());
		firstShipDate.setIsDeleted(user.getIsDeleted());
		BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());
	
		firstShipDate.setCreatedOn(currentTS);
	//	firstShipDate.setModifiedOn(currentTS);
		
		return firstShipDate;

		
	}
	public static RefillHistory refillHistoryRecentShipDateNewLegacyUser(UserDet user,UserProfile userProfile)
	{

		RefillHistory recentShipDate=new RefillHistory();
		recentShipDate.setUser_uid(user.getUuid());
		if(userProfile.getSpMostRecentShipDate()!=null && !(userProfile.getSpFirstShipDate().equals("")) && (userProfile.getSpMostRecentShipDate().length()>1))
		{
			recentShipDate.setRefilled_on(DateTime.changeFormat(userProfile.getSpMostRecentShipDate(),"yyyyMMdd","yyyy-MM-dd"));
		}
		else
		{
			recentShipDate.setRefilled_on(null);
		}
		recentShipDate.setPharmacy_uid(user.getPharmacy_id());
		recentShipDate.setUuid(UUIDGenerator.getUUID());
		recentShipDate.setIsActive(user.getIsActive());
		recentShipDate.setIsDeleted(user.getIsDeleted());
		BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());
	
		recentShipDate.setCreatedOn(currentTS);
	//	firstShipDate.setModifiedOn(currentTS);
		return recentShipDate;

		
	}
	
	public static RefillHistory[] refillHistoryRefillPatternNewLegacyUser(UserDet user,UserProfile userProfile)
	{

		String refillPatternString=userProfile.getCapRefillPattern();
		String refillPatternArray[]=null;
		RefillHistory refillHistoryPatternArray[]=null;
		
		
		if(refillPatternString!=null && !refillPatternString.isEmpty() && !refillPatternString.equals(""))
		{
			//split the string of dates
			refillPatternArray=refillPatternString.split("\\|");
			refillHistoryPatternArray=new RefillHistory[refillPatternArray.length];
			
			for(int ctr=0;ctr<refillPatternArray.length;ctr++)
			{
				System.out.println("creating refill history object for refill pattern for new/legacy user:");
				if(refillPatternArray[ctr]==null || refillPatternArray[ctr].isEmpty())
				{
					RefillHistory refillHistoryinBetweenNull=new RefillHistory();
					refillHistoryinBetweenNull.setUser_uid(user.getUuid());
					refillHistoryinBetweenNull.setPharmacy_uid(user.getPharmacy_id());
					refillHistoryinBetweenNull.setUuid(UUIDGenerator.getUUID());
					refillHistoryinBetweenNull.setIsActive(user.getIsActive());
					refillHistoryinBetweenNull.setIsDeleted(user.getIsDeleted());
					BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());	
					refillHistoryinBetweenNull.setCreatedOn(currentTS);
					refillHistoryinBetweenNull.setRefilled_on(refillPatternArray[ctr]);
					//add the null refilledOn refill history obj  for this ctr value
					//case where yyyymmdd|yyyymmdd||yyyymmdd is found
					//so that hibernate does save a totally null entity
					refillHistoryPatternArray[ctr]=refillHistoryinBetweenNull;
		
					//move to next date in string
					continue;
				}
				//change format of dat
				refillPatternArray[ctr]=DateTime.changeFormat(refillPatternArray[ctr].trim(),"yyyyMMdd","yyyy-MM-dd");
				//create refill history obj with the date
				RefillHistory refillHistory=new RefillHistory();
				refillHistory.setUser_uid(user.getUuid());
				refillHistory.setPharmacy_uid(user.getPharmacy_id());
				refillHistory.setUuid(UUIDGenerator.getUUID());
				refillHistory.setIsActive(user.getIsActive());
				refillHistory.setIsDeleted(user.getIsDeleted());
				BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());	
				refillHistory.setCreatedOn(currentTS);
				refillHistory.setRefilled_on(refillPatternArray[ctr]);
						//add the refill history obj for the date to the array
				refillHistoryPatternArray[ctr]=refillHistory;
				
			}
		}
			else
			{
				//refill pattern either empty string or null
				RefillHistory refillHistory=new RefillHistory();
				refillHistory.setUser_uid(user.getUuid());
				refillHistory.setPharmacy_uid(user.getPharmacy_id());
				refillHistory.setUuid(UUIDGenerator.getUUID());
				refillHistory.setIsActive(user.getIsActive());
				refillHistory.setIsDeleted(user.getIsDeleted());
				BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());	
				refillHistory.setCreatedOn(currentTS);
				refillHistory.setRefilled_on(null);
				refillHistoryPatternArray=new RefillHistory[1];
				refillHistoryPatternArray[0]=refillHistory;
				

			}
	
		return refillHistoryPatternArray;
		
	}
	

	public  RefillHistory refillHistoryFirstShipDateUpdateUser(UserDet user,UserProfile userProfile)
	{// for update with uuid, its receiving userdetfromdb which will have a uuid
		System.out.println("in refillHistoryFirstShipDateUpdateUser. uuid:"+user.getUuid());
		
		Configuration cfg=new Configuration();
		cfg.configure(this.pathToHibernateConfig);
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		Session session=sessionFactory.openSession();
		//retrieve dates in asc order
		String hql_uuid = " FROM RefillHistory where user_uid=:uuidParam order by created_on";
		Query query_uuid = session.createQuery(hql_uuid);
		query_uuid.setParameter("uuidParam",user.getUuid());
		List resultsbasedOnuuid = query_uuid.list();
		if(session!=null)
		{//close session for retrieval from refill history table
			session.flush();
			session.clear();
			session.close();
		}
	
		RefillHistory firstShipDate=new RefillHistory();
		firstShipDate.setUser_uid(user.getUuid());
		firstShipDate.setPharmacy_uid(user.getPharmacy_id());
		firstShipDate.setUuid(UUIDGenerator.getUUID());
		firstShipDate.setIsActive(user.getIsActive());
		firstShipDate.setIsDeleted(user.getIsDeleted());
		firstShipDate.setRefilled_on(userProfile.getSpFirstShipDate());
		BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());
	
	//	firstShipDate.setCreatedOn(currentTS);
		firstShipDate.setModifiedOn(currentTS);
	
		
		if(resultsbasedOnuuid!=null & resultsbasedOnuuid.size()>0)
		{
			 RefillHistory refillHistoryFromDB=(RefillHistory)resultsbasedOnuuid.get(0);
			 System.out.println("[update user first ship date] refill table id for user:"+refillHistoryFromDB.getId());
			firstShipDate.setId(refillHistoryFromDB.getId());
			String firstShipDateFromUserProfile=userProfile.getSpFirstShipDate();
			//to format date in userprofile
			if(userProfile.getSpFirstShipDate()!=null && !userProfile.getSpFirstShipDate().equals(""))
			{
			firstShipDateFromUserProfile=DateTime.changeFormat(firstShipDateFromUserProfile,"yyyyMMdd","yyyy-MM-dd");	
			}	
			else if(userProfile.getSpFirstShipDate()!=null && userProfile.getSpFirstShipDate().equals(""))
			{
				firstShipDateFromUserProfile=null;
			}
			
			//for case of equal dates in db and text file
			if((firstShipDateFromUserProfile==null && refillHistoryFromDB.getRefilled_on()==null)) 
				{
					System.out.println("received first shipment date in file and from db(refill history) are same=null. No data addition to DB ");
					firstShipDate.setRefilled_on(null);
				}
	 			else if(firstShipDateFromUserProfile==null && refillHistoryFromDB!=null)
			{
				System.out.println("first shipment date received is null in text file but not null in table.Update with null ");
				firstShipDate.setRefilled_on(firstShipDateFromUserProfile);
			}
			
			else if(firstShipDateFromUserProfile!=null && refillHistoryFromDB==null)
			{
				System.out.println("first shipment date is NOT null in text file but  null in table.Update with text file value");
				firstShipDate.setRefilled_on(firstShipDateFromUserProfile);
			}
			else if((firstShipDateFromUserProfile!=null && refillHistoryFromDB.getRefilled_on()!=null) && !(refillHistoryFromDB.getRefilled_on().equals(firstShipDateFromUserProfile)))
			{
		System.out.println("received first shipment date in file and from db(refill history) are NOT null and not equal. Update table with text file value ");
		firstShipDate.setRefilled_on(firstShipDateFromUserProfile);

			}
			else if((firstShipDateFromUserProfile!=null && refillHistoryFromDB.getRefilled_on()!=null) && (refillHistoryFromDB.getRefilled_on().equals(firstShipDateFromUserProfile)))
			{
		System.out.println("received first shipment date in file and from db(refill history) are NOT null equal.");
		firstShipDate.setRefilled_on(firstShipDateFromUserProfile);

			}

			
			
		}
			else
			{
				System.out.println("Cannot find the corresponding uuid in first shipment date matching for refill history.");
				UserResponseApp.errorLogs.add("Cannot find the corresponding uuid for matching first shipment date in refill history for update user without UUID:"+new Gson().toJson(user));
			}
		
				return firstShipDate;
		}
	
	public  RefillHistory refillHistoryRecentShipDateUpdateUser(UserDet user,UserProfile userProfile)
	{
		System.out.println("in refillHistoryRecentDateUpdateUser uuid:"+user.getUuid());
		Configuration cfg=new Configuration();
		cfg.configure(this.pathToHibernateConfig);
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		Session session=sessionFactory.openSession();
		String hql_uuid = " FROM RefillHistory where user_uid=:uuidParam order by created_on desc";
		Query query = session.createQuery(hql_uuid);
		query.setParameter("uuidParam",user.getUuid());
		System.out.println("retrieving results from refill history table for update user's recent ship date");
		List resultsbasedOnuuid = query.list();
		RefillHistory recentShipDate=new RefillHistory();
		recentShipDate.setUser_uid(user.getUuid());
		recentShipDate.setPharmacy_uid(user.getPharmacy_id());
		recentShipDate.setUuid(UUIDGenerator.getUUID());
		recentShipDate.setIsActive(user.getIsActive());
		recentShipDate.setIsDeleted(user.getIsDeleted());
		BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());
	
		//recentShipDate.setCreatedOn(currentTS);
		recentShipDate.setModifiedOn(currentTS);
	


		if(resultsbasedOnuuid!=null & resultsbasedOnuuid.size()>0)
		{
			System.out.println("results from refill history for update user's recent ship date received.Access element 0");
			RefillHistory refillHistoryFromDB=(RefillHistory)resultsbasedOnuuid.get(0);
			
			recentShipDate.setId(refillHistoryFromDB.getId());
			
			String recentShipDateUserProfile=userProfile.getSpMostRecentShipDate();
			//format recent ship date from user profile obj
			if(recentShipDateUserProfile!=null && !recentShipDateUserProfile.equals(""))
			{
				recentShipDateUserProfile=DateTime.changeFormat(recentShipDateUserProfile,"yyyyMMdd","yyyy-MM-dd");
			}	
			else
			{
				recentShipDateUserProfile=null;
			}
			
			//set the value of recent ship date
			if((recentShipDateUserProfile==null && refillHistoryFromDB.getRefilled_on()==null))
				{
					System.out.println("received most received shipment date in file and from db are same(null). No data addition to DB ");
					recentShipDate.setRefilled_on(null);
				}
			else if(recentShipDateUserProfile!=null && refillHistoryFromDB.getRefilled_on()!=null && (recentShipDateUserProfile.equals(refillHistoryFromDB.getRefilled_on())))
			{
				System.out.println("received most received shipment date in file and from db are same. No data addition to DB ");
				recentShipDate.setRefilled_on(recentShipDateUserProfile);
	
			}
			else if(recentShipDateUserProfile!=null && refillHistoryFromDB.getRefilled_on()!=null && !(recentShipDateUserProfile.equals(refillHistoryFromDB.getRefilled_on())))
			{
				System.out.println("received most received shipment date in file and from db are not null and not same.update with text file value  ");
				recentShipDate.setRefilled_on(recentShipDateUserProfile);
				recentShipDate.setId(null);
				recentShipDate.setCreatedOn(currentTS);
	
			}
			else if(recentShipDateUserProfile!=null && refillHistoryFromDB.getRefilled_on()==null)
			{
				System.out.println("received most received shipment date in file is not null and from db is null.update with text file value  ");
				recentShipDate.setRefilled_on(recentShipDateUserProfile);
	
			}
			else if(recentShipDateUserProfile==null && refillHistoryFromDB.getRefilled_on()!=null)
			{
				System.out.println("received recent date is null and from db is not null. update recent date in db with null");
				recentShipDate.setRefilled_on(recentShipDateUserProfile);
			}
			
		}
			else
			{
				System.out.println("Cannot find the corresponding uuid in most recent shipment date matching for refill history.");
				UserResponseApp.errorLogs.add("Cannot find the corresponding uuid for matching most recent shipment date in refill history for case of  user without uuid:"+new Gson().toJson(user));
			}
		if(session!=null)
		{
			session.flush();
			session.clear();
			session.close();
		}
	
		return recentShipDate;
		}
	
	
	public static RefillHistory[] refillHistoryRefillPatternUpdateUser(UserDet user,UserProfile userProfile)
	{
		System.out.println("refillHistoryPatternupdateuser");
		Configuration cfg=new Configuration();
		cfg.configure(UserResponseApp.hibernateConfig);
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		Session session=sessionFactory.openSession();
		String hql_uuid = " FROM RefillHistory where user_uid=:uuidParam order by created_on";
		Query query_uuid = session.createQuery(hql_uuid);
		query_uuid.setParameter("uuidParam",user.getUuid());
		List resultsbasedOnuuid = query_uuid.list();
		String refillPatternStrArray[]=null;
		RefillHistory refillPatternFromDBArray[]=null;
		RefillHistory[] refillHistoryPatternArray=null;
		int k=0;
		//refill pattern array= array from user profile
		String refillPatternString=userProfile.getCapRefillPattern();
		if(refillPatternString!=null && !refillPatternString.equals(""))
		{
			refillPatternStrArray=refillPatternString.split("\\|");
			
			
		}
		else
		{
			System.out.println("Refill pattern received in file is empty.acceptable case");
			refillPatternStrArray=null;
		}
		
		if(resultsbasedOnuuid!=null && resultsbasedOnuuid.size()>2 && refillPatternStrArray!=null  && !refillPatternString.equals(""))
		{
			refillPatternFromDBArray=new RefillHistory[resultsbasedOnuuid.size()];
			
			//skip first  record from db because its first ship date 
			for(k=1;k<resultsbasedOnuuid.size();k++)
			{ 
				int lengthDiff=(resultsbasedOnuuid.size()-2)-refillPatternStrArray.length;
				if(lengthDiff==0 && ((k-1)==refillPatternStrArray.length))
				{
					//for case: where caprefill: x_first, x_mid, x_last and db: x_first,x_mid,x_last 
					break;
					
				}
				if(k>refillPatternStrArray.length && lengthDiff>0)
				{
					System.out.println("The refill pattern String in text file has lesser entries than DB's refill pattern string");
					UserResponseApp.errorLogs.add("Logging error & proceeding:The table has more entries for refill pattern than the received String"+new Gson().toJson(user));
					break;
					
				}
				//k-1 because db array starts from first ship date. In text file pattern is in between dates
				refillPatternFromDBArray[k]=(RefillHistory)resultsbasedOnuuid.get(k);
				refillPatternStrArray[k-1]=DateTime.changeFormat(refillPatternStrArray[k-1],"yyyyMMdd","yyyy-MM-dd");
				//side by side comparison
				if(refillPatternStrArray[k-1].equals(refillPatternFromDBArray[k].getRefilled_on()))
				{
					System.out.println("patternStr and db pattern are equal: "+refillPatternStrArray[k-1]+" "+refillPatternFromDBArray[k].getRefilled_on() );
					continue;
					
				}
				else
				{
					System.out.println("patternStr and db pattern are NOT equal:break: "+refillPatternStrArray[k-1]+" "+refillPatternFromDBArray[k].getRefilled_on() );
					
					break;
				}
			}	
			List<RefillHistory> newEntriesinRefill=new ArrayList<RefillHistory>();
		
			System.out.println("continue counter k for refill pattern string for update user from:"+k);
			
			for(int ctr=k-1;ctr<refillPatternStrArray.length;ctr++)
					{
						if(refillPatternStrArray[ctr]==null || refillPatternStrArray[ctr].isEmpty())
						{
							continue;
						}
						RefillHistory refillHistory=new RefillHistory();
						
						if(ctr<refillPatternFromDBArray.length-1)
						{
						refillHistory.setId(refillPatternFromDBArray[ctr+1].getId());	
						}
						//create refill history obj with the date
						refillHistory.setUser_uid(user.getUuid());
						if(refillPatternStrArray[ctr].indexOf("-")>0)
						{
							refillHistory.setRefilled_on(refillPatternStrArray[ctr]);
						}
						else
						{
						refillHistory.setRefilled_on(DateTime.changeFormat(refillPatternStrArray[ctr],"yyyyMMdd","yyyy-MM-dd"));
						}
						refillHistory.setPharmacy_uid(user.getPharmacy_id());
						refillHistory.setUuid(UUIDGenerator.getUUID());
						refillHistory.setIsActive(user.getIsActive());
						refillHistory.setIsDeleted(user.getIsDeleted());
						BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());	
						refillHistory.setCreatedOn(currentTS);
						refillHistory.setModifiedOn(currentTS);;
						//add the refill history obj for the date to the array
							//refillHistoryPatternArray[newEntryCtr]=refillHistory;
						newEntriesinRefill.add(refillHistory);
						System.out.println("new entry for cap refill:"+refillHistory.getRefilled_on());
						
			
					}
			
				refillHistoryPatternArray=new RefillHistory[newEntriesinRefill.size()];
				refillHistoryPatternArray=(RefillHistory[]) newEntriesinRefill.toArray(new RefillHistory[newEntriesinRefill.size()]);
			}
		else if(resultsbasedOnuuid==null)
		{
			System.out.println("[update cap refill pattern for update users]no result from DB using the uuid");
			UserResponseApp.errorLogs.add("[update cap refill pattern for update users]No result from DB table refill history for given uuid for user:"+new Gson().toJson(user));
		}
		else if(resultsbasedOnuuid!=null && resultsbasedOnuuid.size()<3)
		{
			System.out.println("Results from refill history have less than 3 records/rows. is.e one out of first shipment/recent shipment/cap refill is missing");
			UserResponseApp.errorLogs.add("Results from refill history have less than 3 records/rows. is.e one out of first shipment/recent shipment/cap refill is missing for update user:"+new Gson().toJson(user));
		}
			
		else if(resultsbasedOnuuid==null || resultsbasedOnuuid.size()<3)
		{
				System.out.println("[resultsbasedOnuuid!=null && resultsbasedOnuuid.size()>2 returned false in update user's cap refill pattern update. " +new Gson().toJson(user));
				UserResponseApp.errorLogs.add("Either no entries found in refill table or no. of entries for refilled on(null + nonNull) is less than 3 for:" +new Gson().toJson(user));
		}
		if(session!=null)
		{
			session.flush();
			session.clear();
			session.close();
		}
	
		return refillHistoryPatternArray;
	}
		
	public static String getPharmacyInfo(String pharmacyId)
	{
		if(pharmacyId==null || pharmacyId.length()==0)
		{
			return null;
		}
		String pharmacyName="";
		Integer pharmacy_id = null;
		String pharmacyContactNumber="";
		if(pharmacyId.equals("CAYQ7A1"))
		{
			pharmacyName="TLCRx/ModernHEALTH";
			pharmacyContactNumber="1-855-274-1694";
			pharmacy_id=1;
		}
		else if(pharmacyId.equals("CAYQ7A2"))
		{
			pharmacyName="IV Solutions/Maxor";
			pharmacyContactNumber="1-800-658-6046";
			pharmacy_id=2;
		}
		else if(pharmacyId.equals("CAYQ7A3"))
		{
			pharmacyName="Foundation Care";
			pharmacyContactNumber="1-877-291-1122";
			pharmacy_id=3;
		}
		else if(pharmacyId.equals("CAYQ7A4"))
		{
			pharmacyName="Pharmaceutical Specialties, Inc.";
			pharmacyContactNumber="1-800-818-6486";
			pharmacy_id=4;
		}
		
		else if(pharmacyId.equals("CAYQ7A5"))
		{
			pharmacyName="Walgreens Specialty Pharmacy";
			pharmacyContactNumber="1-800-449-2103";
			pharmacy_id=5;
			
		}
						
			return pharmacyName+","+pharmacyContactNumber+","+Integer.toString(pharmacy_id);
		}
	}

