package com.arches.caystonETL;

import java.io.File;
import java.io.FilenameFilter;

public class FileFilter {
    
    private FilenameFilter fileNameFilter;
    
    //specific extension is passed as a parameter to method
    public  FilenameFilter getFilter(final String extension)
    
   {
        FilenameFilter fileNameFilter = new FilenameFilter() {
   
            public boolean accept(File dir, String name) {
               if(name.lastIndexOf('.')>0)
               {
                  // get last index for '.' char
                  int lastIndex = name.lastIndexOf('.');
                  
                  // get extension
                  String str = name.substring(lastIndex);
                  
                  // match path name extension
                  if(str.equals(extension.trim()))
                  {
                     return true;
                  }
               }
               return false;
            }
         };
      
        this.fileNameFilter=fileNameFilter;
        //return the created filter
        return this.fileNameFilter;
    }
}