package com.arches.caystonETL;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import org.hibernate.Query;

import com.arches.model.UserDet;
import com.arches.model.UserProfile;
import com.arches.utilities.DateTime;
import com.google.gson.Gson;

import edu.emory.mathcs.backport.java.util.Arrays;

public class UserObjectValidator {

	List<String> errorList;
	public UserObjectValidator()
	{
		
	}
	public List<String> validate(UserDet user,UserProfile userProfile)
	{
		int userType=user.getUserType();
		if(userType==0)
		{
			errorList=validateLegacyUser(user,userProfile);
			//validate rescriber id
			boolean prescriberResult=verifyPrescriberInfo(user, userProfile);
			if(prescriberResult==Boolean.FALSE)
			{
				errorList.add("Invalid prescriber id:"+userProfile.getPrescriberId()+" for user:"+new Gson().toJson(user));
			}
			//validate cf center id
			boolean cfCenterResult=verifyCFCenterInfo(user, userProfile);
			if(cfCenterResult==Boolean.FALSE)
			{
				errorList.add("Invalid cf center id:"+userProfile.getCfCenter()+" for user:"+new Gson().toJson(user));
			}
		}
		else if(userType==1)
		{
			errorList=validateNewUser(user,userProfile);
			boolean prescriberResult=verifyPrescriberInfo(user, userProfile);
			if(prescriberResult==Boolean.FALSE)
			{
				errorList.add("Invalid prescriber id:"+userProfile.getPrescriberId()+" for user:"+new Gson().toJson(user));
			}
			boolean cfCenterResult=verifyCFCenterInfo(user, userProfile);
			if(cfCenterResult==Boolean.FALSE)
			{
				errorList.add("Invalid cf center id:"+userProfile.getCfCenter()+" for user:"+new Gson().toJson(user));
			}
		}
		else if(userType==2)
		{
			try
			{
				errorList=validateUpdateUser(user,userProfile);
				boolean prescriberResult=verifyPrescriberInfo(user, userProfile);
				if(prescriberResult==Boolean.FALSE)
				{
					errorList.add("Invalid prescriber id:"+userProfile.getPrescriberId()+" for user:"+new Gson().toJson(user));
				}
				
				boolean cfCenterResult=verifyCFCenterInfo(user, userProfile);
				if(cfCenterResult==Boolean.FALSE)
				{
					errorList.add("Invalid cf center id:"+userProfile.getCfCenter()+" for user:"+new Gson().toJson(user));
				}
			}
			catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("parseExc. in validation"+e);
				UserResponseApp.errorLogs.add("Parsing exception in validation of record. Exception:"+e);
				e.printStackTrace();
				  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
			}
		}
		else
		{
			UserResponseApp.errorLogs.add("Invalid User Type");
			
		}

		
		return errorList;
	}
	
	public List<String> validateLegacyUser(UserDet user,UserProfile userProfile)
	{
		System.out.println("--- validating legacy user ---");
		List<String> errorsList=new ArrayList<String>();
		
	try{
		
		if(user.getFirstName().length()>50 || user.getFirstName()==null || user.getFirstName().isEmpty() || (user.getFirstName().matches("[0-9]*")))
		{
			errorsList.add("Invalid first name for legacy user:"+new Gson().toJson(user));
		}																															
		if(user.getLastName().length()>50 || user.getLastName()==null || user.getLastName().isEmpty() || (user.getLastName().matches("[0-9]*")))
				
			{
				errorsList.add(" Invalid last name for legacy user:"+new Gson().toJson(user));
			}
			
		if(user.getEmail()!=null && user.getEmail().length()>0 && ( user.getEmail().length()<4 || user.getEmail().length()>100 || !(user.getEmail().length()>0 && user.getEmail().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))))	
			
		{
			errorsList.add(" Invalid email address for user:"+new Gson().toJson(user));
		}
		if(user.getAddress1().length()>0 && user.getAddress1().length()>100)
		{
			errorsList.add(" Invalid Address1 length for user:"+new Gson().toJson(user));
		}
		if(user.getAddress2().length()>0 && user.getAddress2().length()>100)
		{
			errorsList.add("Invalid Address2 length for user:"+new Gson().toJson(user));
		}
	
		
		if(user.getCity().length()>40)
		{
			errorsList.add("Invalid city field length for user:"+new Gson().toJson(user));
		}
		if(user.getState().length()>40)
		{
			errorsList.add(" Invalid state field length for user:"+new Gson().toJson(user));
		}
		if(user.getZip().length()>10 || !(user.getZip().matches("[0-9\\-]*")))
		{
			errorsList.add("Invalid zip code for user:"+new Gson().toJson(user));
		}
		
		
		if(user.getContactNumber().length()>0 && (user.getContactNumber().length()!=10 || !(user.getContactNumber().length()>0 && user.getContactNumber().matches("[0-9]*"))))
		{
			errorsList.add(" Invalid phone number for user:"+new Gson().toJson(user));
		}
		if(userProfile.getGender()!=null && userProfile.getGender().length()>0 && !(userProfile.getGender().equals("M") || userProfile.getGender().equals("F")))
		{
			errorsList.add(" Invalid Gender value for user:"+new Gson().toJson(user));
		}
		if( userProfile.getDob()!=null && userProfile.getDob().length()>0 && (userProfile.getDob().length()!=8  || !(userProfile.getDob().matches("[0-9]*")  || (DateTime.StringToDate(userProfile.getDob()).after(DateTime.StringToDate(DateTime.getDateTime()))))))
		{
			errorsList.add("Invalid DOB for user:"+new Gson().toJson(user));
		}
		
		if(user.getMediaSourceId()!=null && user.getMediaSourceId().toString().length()>0 && (user.getMediaSourceId().toString().length()!=5 || user.getMediaSourceId().toString().length()==0 || !(user.getMediaSourceId().toString().equals("20002") || user.getMediaSourceId().toString().equals("20003") || user.getMediaSourceId().toString().equals("20004") || user.getMediaSourceId().toString().equals("20005") || user.getMediaSourceId().toString().equals("20006") || user.getMediaSourceId().toString().equals("20007") || user.getMediaSourceId().toString().equals("20008") || user.getMediaSourceId().toString().equals("20009"))  || !(user.getMediaSourceId().toString().matches("[0-9]*"))))
		{
			errorsList.add("Invalid media source ID for user:"+new Gson().toJson(user));
		}
		if(!(userProfile.getRelationship().equals("P") || userProfile.getRelationship().equals("C")))
		{	
			errorsList.add(" Invalid Response for CAYQ1 for user:"+new Gson().toJson(user));
		 }
		if(user.getCapPatientId()==null || user.getCapPatientId().length()==0)
		{
			errorsList.add(" Response Required for CAYQ2 for user:"+new Gson().toJson(user));
		    
		}
		
		if(user.getCapEnrollmentDate().length()!=8  || (DateTime.StringToDate(user.getCapEnrollmentDate()).before(DateTime.StringToDate("20090101"))))
		{
	 		errorsList.add("Invalid response for CAYQ3 for user:"+new Gson().toJson(user));
	 	   	
		}
	 	if(userProfile.getSpFirstShipDate()!=null && userProfile.getSpFirstShipDate().length()>0 && (userProfile.getSpFirstShipDate().length()!=8 || DateTime.StringToDate(userProfile.getSpFirstShipDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getSpFirstShipDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ4 for user:"+new Gson().toJson(user));
    	}
    	if(userProfile.getSpMostRecentShipDate()!=null && userProfile.getSpMostRecentShipDate().length()>0 && (userProfile.getSpMostRecentShipDate().length()!=8 || DateTime.StringToDate(userProfile.getSpMostRecentShipDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getSpMostRecentShipDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ5 for user:"+new Gson().toJson(user));
    	}
    	
    	if(userProfile.getCapRefillPattern()!=null && userProfile.getCapRefillPattern().length()!=0 && !(userProfile.getCapRefillPattern().matches("[0-9]{8}(\\|[0-9]{8})*")))
    	{
    		
    		errorsList.add(" Invalid response format for CAYQ6 for user:"+new Gson().toJson(user));
    	    		
    	}
    	if(((userProfile.getSpFirstShipDate()==null || userProfile.getSpFirstShipDate().equals("")) && (userProfile.getSpMostRecentShipDate()==null ||userProfile.getSpMostRecentShipDate().equals(""))) && (userProfile.getCapRefillPattern()!=null && !userProfile.getCapRefillPattern().equals("")))
    	{
    		errorsList.add(" Invalid response  for CAYQ6(CAYQ4 and CAYQ5 are empty but CAYQ6 is non empty) for user:"+new Gson().toJson(user));
        	
    	}
    	if(userProfile.getCapRefillPattern()!=null && userProfile.getCapRefillPattern().length()!=0 && !(userProfile.getSpFirstShipDate().equals(userProfile.getSpMostRecentShipDate())))
    	{
    		String refillPattern[]=userProfile.getCapRefillPattern().split("\\|");
    		boolean duplicateFlag=false;
    		Set<String> uniquUsers = new HashSet<String>();

            for (int i = 0; i < refillPattern.length; i++) {
                if (!uniquUsers.add(refillPattern[i]))
                    {
                		duplicateFlag=true;
                    }
            	
                }
          if(duplicateFlag==true)
    		{
    			errorsList.add("Duplicate values repeated in cap refill Pattern for user:"+new Gson().toJson(user));
    		}
          
    	}


    	if(user.getPharmacy()!=null && user.getPharmacy().length()>0 && !(user.getPharmacy().equals("CAYQ7A1") || user.getPharmacy().equals("CAYQ7A2") ||  user.getPharmacy().equals("CAYQ7A3") ||  user.getPharmacy().equals("CAYQ7A4") ||  user.getPharmacy().equals("CAYQ7A5") || user.getPharmacy().equals("CAYQ7A6")))
    	{
    		errorsList.add(" Invalid Response for CAYQ7 for user:"+new Gson().toJson(user));
    	}
    	if(user.getPapPatientStatus().length()>0 &&  !(user.getPapPatientStatus().equals("CAYQ8A1") || user.getPapPatientStatus().equals("CAYQ8A2")))
    	{
    		errorsList.add(" Invalid Response for CAYQ8 for user:"+new Gson().toJson(user));
    	}
    	
    	if(userProfile.getPapRxFirstDispenseDate().length()>0 && (userProfile.getPapRxFirstDispenseDate().length()!=8 || DateTime.StringToDate(userProfile.getPapRxFirstDispenseDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getPapRxFirstDispenseDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ9 for user:"+new Gson().toJson(user));	    
    	}
      
    	if(userProfile.getPapRxDispenseActivity()!=null && userProfile.getPapRxDispenseActivity().length()>0 && !(userProfile.getPapRxDispenseActivity().matches("[0-9]{8}(\\|[0-9]{8})*")))
    	{
    		errorsList.add(" Invalid Response format for CAYQ10 for user:"+new Gson().toJson(user));
    	    		
    	}
    	if( userProfile.getPapRxLatestDispenseDate().length()>0 && (userProfile.getPapRxLatestDispenseDate().length()!=8 || DateTime.StringToDate(userProfile.getPapRxLatestDispenseDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getPapRxLatestDispenseDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add("Invalid response for CAYQ11 for user:"+new Gson().toJson(user));
    	}
    	if(user.getCapHasInsurance().length()>0 && !(user.getCapHasInsurance().equals("CAYQ12A1") || user.getCapHasInsurance().equals("CAYQ12A2") ))
    	{
    		errorsList.add(" Invalid Response for CAYQ12 for user:"+new Gson().toJson(user));
    	}
    	if(!(user.getCapPatientStatus().equals("CAYQ13A1") || user.getCapPatientStatus().equals("CAYQ13A2") || user.getCapPatientStatus().equals("CAYQ13A3") ))
    	{
    		errorsList.add(" Invalid/Missing Response for CAYQ13  for user:"+new Gson().toJson(user));
    	}
    	if(user.getCapFormId().length()==0 || !user.getCapFormId().matches("[A-za-z_0-9]*"))
    	{
    		errorsList.add("Response expected for CAYQ14 for user:"+new Gson().toJson(user));
	    	
    	}
     	if(!((user.getBrandedOptCheck().compareTo(new Byte("0"))==0) || (user.getBrandedOptCheck().compareTo(new Byte("1"))==0)))
    	{
    		errorsList.add("Invalid Response for CAYQ15 for user:"+new Gson().toJson(user));
    	}
     	if(user.getRemainingRefillCount().equals("CAYQ16") && !user.getRemainingRefillCount().equals("0"))
     	{
     		errorsList.add(" Zero Response expected for CAYQ16 for user:"+new Gson().toJson(user));    
     	}
     	if(user.getOptInDate().length()!=8 || DateTime.StringToDate(user.getOptInDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(user.getOptInDate()).before(DateTime.StringToDate("20090101"))))
    	{
    		errorsList.add(" Invalid or missing response for CAYQ17 for user:"+new Gson().toJson(user));
    	}
     	if(user.getOptOutDate().length()>0 && (user.getOptOutDate().length()!=8 || DateTime.StringToDate(user.getOptOutDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(user.getOptOutDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ18 for user:"+new Gson().toJson(user));
    	}
   

    	

		
	}
	catch(Exception e)
	{
	System.out.println("Legacy exception:"+e);	
		UserResponseApp.errorLogs.add("Exception in validating legacy user record's fields.Exception:"+e);
		  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
	

	}
	return errorsList;
	
	}
	
	
	
	public List<String> validateNewUser(UserDet user,UserProfile userProfile)
	{
		System.out.println("--- validating new user ---");
		
		//list for recording errors
		List<String> errorsList=new ArrayList<String>();
	try
	{
		if(user.getFirstName().length()>50 || user.getFirstName()==null || user.getFirstName().isEmpty() || (user.getFirstName().matches("[0-9]*")))
		{
			errorsList.add(" Invalid first name for new user "+new Gson().toJson(user));
		}
		if(user.getLastName().length()>50 || user.getLastName()==null || user.getLastName().isEmpty() || (user.getLastName().matches("[0-9]*")))
				
			{
				errorsList.add(" Invalid last name for new user:"+new Gson().toJson(user));
			}
			
		if(user.getEmail()!=null && user.getEmail().length()>0 &&  (user.getEmail().length()<4 || user.getEmail().length()>100 || !((user.getEmail().length()!=0) && (user.getEmail().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")))))	
			
		{
			errorsList.add(" Invalid email address for user:"+new Gson().toJson(user));
		}
		if(user.getAddress1().length()>0 && user.getAddress1().length()>100)
		{
			errorsList.add(" Invalid address length for user:"+new Gson().toJson(user));
		}
		if(user.getCity().length()>40)
		{
			errorsList.add("Invalid city field length for user:"+new Gson().toJson(user));
		}
		if(user.getState().length()>40)
		{
			errorsList.add(" Invalid state field length for user:"+new Gson().toJson(user));
		}
		if(user.getZip().length()>10 || (!(user.getZip().matches("[0-9\\-]*"))))
		{
			errorsList.add(" Invalid zip code for user:"+new Gson().toJson(user));
		}
		if((user.getContactNumber().length()>0) && (user.getContactNumber().length()!=10 || !(user.getContactNumber().matches("[0-9]*"))))
		{
			errorsList.add(" Invalid phone number for user:"+new Gson().toJson(user));
		}
		if(userProfile.getGender()!=null && userProfile.getGender().length()>0 && !(userProfile.getGender().equals("M") || userProfile.getGender().equals("F")))
	{
			errorsList.add(" Invalid gender value for user:"+new Gson().toJson(user));
		}
		if( userProfile.getDob()!=null && userProfile.getDob().length()>0 && (userProfile.getDob().length()!=8 || userProfile.getDob().length()==0 || !(userProfile.getDob().matches("[0-9]*"))  || (DateTime.StringToDate(userProfile.getDob()).after(DateTime.StringToDate(DateTime.getDateTime())))))
		{
			errorsList.add(" Invalid DOB for user:"+new Gson().toJson(user));
		}
		if(user.getMediaSourceId()!=null && user.getMediaSourceId().toString().length()>0 && (user.getMediaSourceId().toString().length()!=5 || !(user.getMediaSourceId().toString().equals("20002") || user.getMediaSourceId().toString().equals("20003") || user.getMediaSourceId().toString().equals("20004") || user.getMediaSourceId().toString().equals("20005") || user.getMediaSourceId().toString().equals("20006") || user.getMediaSourceId().toString().equals("20007") || user.getMediaSourceId().toString().equals("20008") || user.getMediaSourceId().toString().equals("20009")) || !(user.getMediaSourceId().toString().matches("[0-9]*"))))
		{
			errorsList.add(" Invalid media source ID for user:"+new Gson().toJson(user));
		}
		if(!(userProfile.getRelationship().equals("P") || userProfile.getRelationship().equals("C")))
    	{
    		errorsList.add("Invalid Response for CAYQ1 for user:"+new Gson().toJson(user));
    	}
    	if(user.getCapPatientId().length()==0)
    	{
    		errorsList.add("Response Required for CAYQ2 for user:"+new Gson().toJson(user));
    	}
    	if(user.getCapEnrollmentDate().length()!=8 || (DateTime.StringToDate(user.getCapEnrollmentDate()).before(DateTime.StringToDate("20090101"))))
    	{
    		errorsList.add(" Invalid response for CAYQ3 for user:"+new Gson().toJson(user));
    	}
    	if(userProfile.getSpFirstShipDate()!=null && userProfile.getSpFirstShipDate().length()>0 &&  (userProfile.getSpFirstShipDate().length()!=8 || DateTime.StringToDate(userProfile.getSpFirstShipDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getSpFirstShipDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ4 for user:"+new Gson().toJson(user));
    	}
    	if(userProfile.getSpMostRecentShipDate()!=null && userProfile.getSpMostRecentShipDate().length()>0 && (userProfile.getSpMostRecentShipDate().length()!=8 || DateTime.StringToDate(userProfile.getSpMostRecentShipDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getSpMostRecentShipDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ5 for user:"+new Gson().toJson(user));
    	}
    	if(userProfile.getCapRefillPattern()!=null && userProfile.getCapRefillPattern().length()>0 && !(userProfile.getCapRefillPattern().matches("[0-9]{8}(\\|[0-9]{8})*")))
    	{
    		errorsList.add(" Invalid response format for CAYQ6 for user:"+new Gson().toJson(user));
    	    		
    	}
    	if(((userProfile.getSpFirstShipDate()==null || userProfile.getSpFirstShipDate().equals("")) && (userProfile.getSpMostRecentShipDate()==null ||userProfile.getSpMostRecentShipDate().equals(""))) && (userProfile.getCapRefillPattern()!=null && !userProfile.getCapRefillPattern().equals("")))
    	{
    		errorsList.add(" Invalid response  for CAYQ6(CAYQ4 and CAYQ5 are empty but CAYQ6 is non empty) for user:"+new Gson().toJson(user));
        	
    	}
    
    	if(userProfile.getCapRefillPattern()!=null && userProfile.getCapRefillPattern().length()!=0 && !(userProfile.getSpFirstShipDate().equals(userProfile.getSpMostRecentShipDate())))
    	{
    		String refillPattern[]=userProfile.getCapRefillPattern().split("\\|");
    		boolean duplicateFlag=false;
    		Set<String> uniquUsers = new HashSet<String>();

            for (int i = 0; i < refillPattern.length; i++) {
                if (!uniquUsers.add(refillPattern[i]))
                    duplicateFlag=true;
            	System.out.println("duplicate cap refill flag set to true");
                }
          if(duplicateFlag==true)
    		{
    			errorsList.add("Duplicate values repeated in cap refill Pattern for user:"+new Gson().toJson(user));
    		}


    	}


    	if(user.getPharmacy()!=null && user.getPharmacy().length()>0 &&  !(user.getPharmacy().equals("CAYQ7A1") || user.getPharmacy().equals("CAYQ7A2") ||  user.getPharmacy().equals("CAYQ7A3") ||  user.getPharmacy().equals("CAYQ7A4") ||  user.getPharmacy().equals("CAYQ7A5") || user.getPharmacy().equals("CAYQ7A6") ))
    	{
    		errorsList.add(" Invalid Response for CAYQ7 for user:"+new Gson().toJson(user));
    	}
    	if(user.getPapPatientStatus().length()>0 &&  !(user.getPapPatientStatus().equals("CAYQ8A1") || user.getPapPatientStatus().equals("CAYQ8A2")))
    	{
    		errorsList.add(" Invalid Response for CAYQ8 for user:"+new Gson().toJson(user));
    	}
    	if(userProfile.getPapRxFirstDispenseDate().length()>0 && (userProfile.getPapRxFirstDispenseDate().length()!=8 || DateTime.StringToDate(userProfile.getPapRxFirstDispenseDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getPapRxFirstDispenseDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add("Invalid response for CAYQ9 for user:"+new Gson().toJson(user));	    
    	}
    	
    	if(userProfile.getPapRxDispenseActivity()!=null && userProfile.getPapRxDispenseActivity().length()>0 && !(userProfile.getPapRxDispenseActivity().matches("[0-9]{8}(\\|[0-9]{8})*")))
    	{
    		errorsList.add(" Invalid Response format for CAYQ10 for user:"+new Gson().toJson(user));
    	    		
    	}
    	if(userProfile.getPapRxLatestDispenseDate().length()>0 && (userProfile.getPapRxLatestDispenseDate().length()!=8 || DateTime.StringToDate(userProfile.getPapRxLatestDispenseDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getPapRxLatestDispenseDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid Response for CAYQ11 for user:"+new Gson().toJson(user));
    	}
    	if(user.getCapHasInsurance().length()>0 && !(user.getCapHasInsurance().equals("CAYQ12A1") || user.getCapHasInsurance().equals("CAYQ12A2") ))
    	{
    		errorsList.add("Invalid Response for CAYQ12 for user:"+new Gson().toJson(user));
    	}
    	if(!(user.getCapPatientStatus().equals("CAYQ13A1") || user.getCapPatientStatus().equals("CAYQ13A2") || user.getCapPatientStatus().equals("CAYQ13A3") ))
    	{
    		errorsList.add("Invalid or Missing Response for CAYQ13  for user:"+new Gson().toJson(user));
    	}
    	if(user.getCapFormId().length()==0)
    	{
    		errorsList.add(" Response expected for CAYQ14 for user:"+new Gson().toJson(user));
	    	
    	}
     	if(!((user.getBrandedOptCheck().compareTo(new Byte("0"))==0) || (user.getBrandedOptCheck().compareTo(new Byte("1"))==0)))
    	{
    		errorsList.add(" Invalid Response for CAYQ15 for user:"+new Gson().toJson(user));
    	}
     	if(user.getRemainingRefillCount()!=null && !Integer.toString(user.getRemainingRefillCount()).matches("[0-9]*"))
     			{
     		errorsList.add(" Invalid Response  for CAYQ16 for user:"+new Gson().toJson(user));
    	    
     			}
     	if(user.getOptInDate().length()>0 && (user.getOptInDate().length()!=8 || DateTime.StringToDate(user.getOptInDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(user.getOptInDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add("Invalid or missing response for CAYQ17 for user:"+new Gson().toJson(user));
    	}
     	if(user.getOptOutDate().length()>0 && (user.getOptOutDate().length()!=8 || DateTime.StringToDate(user.getOptOutDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(user.getOptOutDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add("Invalid response for CAYQ18 for user:"+new Gson().toJson(user));
    	}
   

	}
	
	catch(Exception e)
	{
		System.out.println("Exception in vlidation"+e);
		UserResponseApp.errorLogs.add("Exception in validating new user record's fields.Exception:"+e);
		  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);

	}
	System.out.println("returning errorslist for new user."+errorsList);
	return errorsList;
}
	
	
	public List<String> validateUpdateUser(UserDet user,UserProfile userProfile) throws ParseException
	{
		System.out.println("--- validating update user ---");
		List<String> errorsList=new ArrayList<String>();
		
	try
	{	
		if(user.getFirstName().length()>50 || user.getFirstName()==null || user.getFirstName().isEmpty() || (user.getFirstName().matches("0-9]*")))
		{
			errorsList.add(" Invalid first name for update user:"+new Gson().toJson(user));
		}
		if(user.getLastName().length()>50 || user.getLastName()==null || user.getLastName().isEmpty() || (user.getLastName().matches("[0-9]*")))
				
			{
				errorsList.add(" invalid last name for update user:"+new Gson().toJson(user));
			}
			
		if(user.getEmail()!=null && user.getEmail().length()>0 && (user.getEmail().length()<4 || user.getEmail().length()>100 || !(user.getEmail().length()>0 && user.getEmail().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))))	
			
		{
			errorsList.add(" Invalid email address for update user:"+new Gson().toJson(user));
		}
		if(user.getAddress1().length()>0 && user.getAddress1().length()>100)
		{
			errorsList.add(" Invalid address1 length for update user:"+new Gson().toJson(user));
		}
		if(user.getAddress2().length()>0 && user.getAddress2().length()>100)
		{
			errorsList.add(" Invalid address2 length for user:"+new Gson().toJson(user));
		}
	
		if(user.getCity().length()>40)
		{
			errorsList.add(" invalid city field length for length:"+new Gson().toJson(user));
		}
		if(user.getState().length()>40)
		{
			errorsList.add(" Invalid state field length for length:"+new Gson().toJson(user));
		}
		if(user.getZip().length()>10 || !(user.getZip().matches("[0-9\\-]*")))
		{
			errorsList.add(" Invalid zip code for user:"+new Gson().toJson(user));
		}
		if(user.getContactNumber().length()>0 && (user.getContactNumber().length()!=10 || !(user.getContactNumber().length()>0 && user.getContactNumber().matches("[0-9]*"))))
		{
			errorsList.add(" Invalid phone number for user:"+new Gson().toJson(user));
		}
		if(userProfile.getGender()!=null && userProfile.getGender().length()>0 && !(userProfile.getGender().equals("M") || userProfile.getGender().equals("F")))
{
			errorsList.add(" Invalid gender value for user:"+new Gson().toJson(user));
		}
		if( userProfile.getDob()!=null && userProfile.getDob().length()>0 && (userProfile.getDob().length()!=8 || (userProfile.getDob().length()==0) || !(userProfile.getDob().matches("[0-9]*")  || (DateTime.StringToDate(userProfile.getDob()).after(DateTime.StringToDate(DateTime.getDateTime()))))))
		{
			errorsList.add("Invalid DOB for user:"+new Gson().toJson(user));
		}
		if(user.getMediaSourceId()!=null && user.getMediaSourceId().toString().length()>0 && (user.getMediaSourceId().toString().length()!=5 || user.getMediaSourceId().toString().length()==0 || !(user.getMediaSourceId().toString().equals("20002") || user.getMediaSourceId().toString().equals("20003") || user.getMediaSourceId().toString().equals("20004") || user.getMediaSourceId().toString().equals("20005") || user.getMediaSourceId().toString().equals("20006") || user.getMediaSourceId().toString().equals("20007") || user.getMediaSourceId().toString().equals("20008") || user.getMediaOriginSrc().equals("20009"))  || !(user.getMediaSourceId().toString().matches("[0-9]*"))))
		{
			errorsList.add("Invalid media source ID for user:"+new Gson().toJson(user));
		}
		if(user.getUuid()!=null && user.getUuid().trim().length()>0 && user.getUuid().trim().length()!=36)
		{
			errorsList.add(" Invalid Arches Patient ID for user:"+new Gson().toJson(user));
		}
		if(!(userProfile.getRelationship().equals("P") || userProfile.getRelationship().equals("C") ))
    	{
    		errorsList.add("Invalid Response for CAYQ1 for user:"+new Gson().toJson(user));
    	}
    	if(user.getCapPatientId().length()==0)
    	{
    		errorsList.add(" Response Required for CAYQ2 for user:"+new Gson().toJson(user));
    	}
    	if(user.getCapEnrollmentDate().length()!=8 ||  (DateTime.StringToDate(user.getCapEnrollmentDate()).before(DateTime.StringToDate("20090101"))))
    	{
    		errorsList.add("invalid response for CAYQ3 for user:"+new Gson().toJson(user));
    	}
    	if(userProfile.getSpFirstShipDate()!=null && userProfile.getSpFirstShipDate().length()>0 && (userProfile.getSpFirstShipDate().length()!=8 || DateTime.StringToDate(userProfile.getSpFirstShipDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getSpFirstShipDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add("invalid response for CAYQ4 for user:"+new Gson().toJson(user));
    	}
    	if(userProfile.getSpMostRecentShipDate()!=null && userProfile.getSpMostRecentShipDate().length()>0 && (userProfile.getSpMostRecentShipDate().length()!=8 || DateTime.StringToDate(userProfile.getSpMostRecentShipDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getSpMostRecentShipDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add("invalid response for CAYQ5 for user:"+new Gson().toJson(user));
    	}
    
    	if(userProfile.getCapRefillPattern()!=null && userProfile.getCapRefillPattern().length()!=0 && !(userProfile.getSpFirstShipDate().equals(userProfile.getSpMostRecentShipDate())) &&  !(userProfile.getCapRefillPattern().matches("[0-9]{8}(\\|[0-9]{8})*")))
    	{
    		errorsList.add(" Invalid response format for CAYQ6 for user:"+new Gson().toJson(user));
    	}
    	if(((userProfile.getSpFirstShipDate()==null || userProfile.getSpFirstShipDate().equals("")) && (userProfile.getSpMostRecentShipDate()==null ||userProfile.getSpMostRecentShipDate().equals(""))) && (userProfile.getCapRefillPattern()!=null && !userProfile.getCapRefillPattern().equals("")))
    	{
    		errorsList.add(" Invalid response  for CAYQ6(CAYQ4 and CAYQ5 are empty but CAYQ6 is non empty) for user:"+new Gson().toJson(user));
        	
    	}
    
    	if(userProfile.getCapRefillPattern()!=null && userProfile.getCapRefillPattern().length()!=0 && !(userProfile.getSpFirstShipDate().equals(userProfile.getSpMostRecentShipDate())))
    	{
    		String refillPattern[]=userProfile.getCapRefillPattern().split("\\|");
    		boolean duplicateFlag=false;
    		Set<String> uniquUsers = new HashSet<String>();

            for (int i = 0; i < refillPattern.length; i++) {
                if (!uniquUsers.add(refillPattern[i]))
                {      duplicateFlag=true;
            	System.out.println("duplicate cap refill flag set to true");
                }
            }
          if(duplicateFlag==true)
    		{
    			errorsList.add("Duplicate values repeated in cap refill Pattern for user:"+new Gson().toJson(user));
    		}


    	}

    	
    	if(user.getPharmacy()!=null && user.getPharmacy().length()>0 &&  !(user.getPharmacy().equals("CAYQ7A1") || user.getPharmacy().equals("CAYQ7A2") ||  user.getPharmacy().equals("CAYQ7A3") ||  user.getPharmacy().equals("CAYQ7A4") ||  user.getPharmacy().equals("CAYQ7A5") || user.getPharmacy().equals("CAYQ7A6")))
    	{
    		errorsList.add(" Invalid Response for CAYQ7 for user:"+new Gson().toJson(user));
    	}
    	if(user.getPapPatientStatus().length()>0 &&  !(user.getPapPatientStatus().equals("CAYQ8A1") || user.getPapPatientStatus().equals("CAYQ8A2")))
    	{
    		errorsList.add(" Invalid Response for CAYQ8 for user:"+new Gson().toJson(user));
    	}
    	if(userProfile.getPapRxFirstDispenseDate().length()>0 && (userProfile.getPapRxFirstDispenseDate().length()!=8 || DateTime.StringToDate(userProfile.getPapRxFirstDispenseDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getPapRxFirstDispenseDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ9 for user:"+new Gson().toJson(user));	    	
    		}
    	
    	if(userProfile.getPapRxDispenseActivity()!=null && userProfile.getPapRxDispenseActivity().length()>0 && !(userProfile.getPapRxDispenseActivity().matches("[0-9]{8}(\\|[0-9]{8})*")))
    	{
    		errorsList.add(" Invalid Response format for CAYQ10 for user:"+new Gson().toJson(user));
    	    		
    	}
		
    	
    	if(userProfile.getPapRxLatestDispenseDate().length()>0 && (userProfile.getPapRxLatestDispenseDate().length()!=8 || DateTime.StringToDate(userProfile.getPapRxLatestDispenseDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(userProfile.getPapRxLatestDispenseDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ11 for user:"+new Gson().toJson(user));
    	}
	
      if(user.getCapHasInsurance().length()>0 && !(user.getCapHasInsurance().equals("CAYQ12A1") || user.getCapHasInsurance().equals("CAYQ12A2") ))
    	{
    		errorsList.add(" Invalid Response for CAYQ12 for user:"+new Gson().toJson(user));
    	}
    	if(!(user.getCapPatientStatus().equals("CAYQ13A1") || user.getCapPatientStatus().equals("CAYQ13A2") || user.getCapPatientStatus().equals("CAYQ13A3") ))
    	{
    		errorsList.add("Invalid/Missing Response for CAYQ13  for user:"+new Gson().toJson(user));
    	}
    	
    	if(user.getCapFormId().length()==0)
    	{
    		errorsList.add(" Response expected for CAYQ14 for user:"+new Gson().toJson(user));
	    	
    	}
    	
     	if(!((user.getBrandedOptCheck().compareTo(new Byte("0"))==0) || (user.getBrandedOptCheck().compareTo(new Byte("1"))==0)))
    	{
    		errorsList.add(" Invalid Response for CAYQ15 for user:"+new Gson().toJson(user));
    	}
     	if(!Integer.toString(user.getRemainingRefillCount()).matches("[0-9]*"))
     			{	
     		errorsList.add("Invalid Response for CAYQ16 for user:"+new Gson().toJson(user));
    	    
     			}
    	
     	if(user.getOptInDate().length()!=8 || DateTime.StringToDate(user.getOptInDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(user.getOptInDate()).before(DateTime.StringToDate("20090101"))))
    	{
    		errorsList.add("Invalid or missing response for CAYQ17 for user:"+new Gson().toJson(user));
    	}
    	
     	if(user.getOptOutDate().length()>0 && (user.getOptOutDate().length()!=8 || DateTime.StringToDate(user.getOptOutDate()).after(DateTime.StringToDate(DateTime.getDateTime())) || (DateTime.StringToDate(user.getOptOutDate()).before(DateTime.StringToDate("20090101")))))
    	{
    		errorsList.add(" Invalid response for CAYQ18 for user:"+new Gson().toJson(user));
    	}

	}
	catch(Exception e)
	{
		System.out.println("!!!Exc."+e);
		UserResponseApp.errorLogs.add("Exception in validating update user record's fields.Exception:"+e);
		  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);

	}
	System.out.println("returning validaation errors for update user");
return errorsList;	
}
	
	public static boolean verifyPrescriberInfo(UserDet user,UserProfile userProfile)
	{

		String prescriberId=userProfile.getPrescriberId();
		
		String hql_prescriber = "SELECT id FROM Prescriber where prescriberId=:prescriberIdParam";
		Query query_prescriber = UserResponseApp.session.createQuery(hql_prescriber);
		query_prescriber.setParameter("prescriberIdParam",prescriberId);
		List prescriber_result=query_prescriber.list();
		if(prescriber_result!=null && prescriber_result.size()>0)
		{
			System.out.println("valid prescriber id");
			return Boolean.TRUE;
		}
		else
		{
			System.out.println("invalid prescriber id");
			return Boolean.FALSE;
		}
		
	}

	public static boolean verifyCFCenterInfo(UserDet user,UserProfile userProfile)
	{

		
		String cf_center=userProfile.getCfCenter();
		String hql_cfCenter = "SELECT id FROM CFCenter where organizationId=:param";
		Query query_cfCenter = UserResponseApp.session.createQuery(hql_cfCenter);
		query_cfCenter.setParameter("param",cf_center);
		
		List result=query_cfCenter.list();
		if(result!=null && result.size()>0)
		{
			System.out.println("valid cf center id");
			return Boolean.TRUE;
		}
		else
		{
			System.out.println("invalid cf center id");
			return Boolean.FALSE;
		}
		
	}


}