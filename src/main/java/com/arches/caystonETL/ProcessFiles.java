package com.arches.caystonETL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;

import javax.swing.plaf.SliderUI;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.arches.model.UserDet;
import com.arches.model.UserListsWrapper;
import com.arches.model.UserObjectWrapper;
import com.arches.model.UserProfile;
import com.arches.utilities.DateTime;
import com.arches.utilities.UUIDGenerator;
import com.fasterxml.jackson.annotation.JsonFormat.Value;
import com.google.gson.Gson;


public class ProcessFiles {
	File[] listOfFiles;
	CaystonClient caystonClient=null;
	public static long  countOfRecords=0;
	
	//constructor initializes list Of files
	public ProcessFiles(String location)
	{
		//use .txt file filter to scan decrypted files
		   FileFilter ff=new FileFilter();
	        FilenameFilter fileNameFilter=ff.getFilter(".txt");
	        
	        File dir=new File(location);
	              try{
	                   //get list of files based on defined filter
	               this.listOfFiles=dir.listFiles(fileNameFilter);
			        
	                 }
	              catch(Exception e)
	              {
	            	  UserResponseApp.errorLogs.add("Exception in getting list of files from directory for processing."+e);
	            	  System.out.println("Exception in Process files:"+e);	 
	              }
	              caystonClient=new CaystonClient();
	   
	}

	public File[] getListOfFiles()
	{
		//return list of files obtained in constructor
		return listOfFiles;
	
	}
	public boolean processRecords()
	{
	
		List<UserDet> userDetList=new ArrayList<UserDet>();
		List<UserProfile> userProfileList=new ArrayList<UserProfile>();
		UserListsWrapper userListsWrapper=new UserListsWrapper();
		boolean result=true;
		for(int i=0;i<listOfFiles.length;i++)
		{
			System.out.println("in process records-iterating files");
			if(i==listOfFiles.length-1)
			{
				UserResponseApp.stringOfFileNamesForSummary=UserResponseApp.stringOfFileNamesForSummary.concat(listOfFiles[i].getName()+".");
				
			}
			else
			{
			UserResponseApp.stringOfFileNamesForSummary=UserResponseApp.stringOfFileNamesForSummary.concat(listOfFiles[i].getName()+" ,");
			}
		//	boolean isValid=validateFile(listOfFiles[i].getName(),1);
				int lineCounter=0;
			FileInputStream inputStream = null;
			Scanner sc = null;
				try 
				{
					inputStream = new FileInputStream(listOfFiles[i].getAbsolutePath());
					sc = new Scanner(inputStream, "UTF-8");
					while (sc.hasNextLine())
					{
						List<String> validationErrorsList=new ArrayList<String>();
						String line = sc.nextLine();
						if(line==null || line.length()==0 || line.length()==1)
						{
							System.out.println("Empty record...continue");
							continue;
						}
						lineCounter++;
						System.out.println("========================Processing Record# "+lineCounter+" from File:"+listOfFiles[i].getName()+"============================");
						UserResponseApp.errorLogs.add("========================Processing Record# "+lineCounter+" from File:"+listOfFiles[i].getName()+"============================");
							//send line for processing and receive two wrapped objects
							UserObjectWrapper userObjectWrapper=processRecord(line);
							if(userObjectWrapper==null)
							{
								System.out.println("userObjectWrapper received after record processing is null");
								UserResponseApp.errorLogs.add("userObjectWrapper[userDet+userProfile object] received after record processing is null.Cannot check for validations or register it");
							}
							//get object one: UserDet 
							UserObjectValidator userObjectValidator=new UserObjectValidator();
							UserDet userDet=userObjectWrapper.getUserDet();
							UserProfile userProfile=userObjectWrapper.getUserProfile();
							
							List<String> receivedValidationErrors=userObjectValidator.validate(userDet, userProfile);
								if(receivedValidationErrors!=null)
									{
											validationErrorsList.addAll(receivedValidationErrors);
											}
										
							
		  	    	System.out.println("checking validation errors list...");
		  	    			if(validationErrorsList==null || validationErrorsList.size()==0)
		  	    			{
		  	    				System.out.println("No errors in validation..calling register");
		  	    					String response=caystonClient.registerUsers(userDet,userProfile);
		  	    							  	    			//no need to handle/validate response. Its handled in cayston client
		  	    			}
		  	    			else
		  	    			{
		  	    				CaystonClient.validationErrorRecords++;
		  	    				UserResponseApp.errorLogs.add("Validation Errors found:"+validationErrorsList);
								
		  	    			}
		  	    			line=null;
		  	    			//if(lineCounter%50==0)
		  	    			//{
		  	    			//	System.gc();
		  	    		//	}
						}
					//lines processing done. Store no. in array corresponding to the file
					UserResponseApp.recordsCountsForSummary=UserResponseApp.recordsCountsForSummary+lineCounter;
							// to handle : Scanner suppresses exceptions
							if (sc.ioException() != null)
							{
								//throw sc.ioException();
								result=false;
								UserResponseApp.errorLogs.add("Error in closing java scanner for reading lines from file:"+listOfFiles[i].getAbsolutePath()+". Exception:"+sc.ioException());
								  UserResponseApp.caystonETLLogger.log(Level.SEVERE,sc.ioException().getMessage(),sc.ioException());
							}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result=false;
				UserResponseApp.errorLogs.add("Error in finding/reading file(s):"+listOfFiles[i].getAbsolutePath()+" Exception:"+e);
				  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result=false;
				UserResponseApp.errorLogs.add("IO exception while reading decrypted file(s):"+listOfFiles[i].getAbsolutePath()+".Exception:"+e);
				  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
			}
				finally 
				{
					
						if (inputStream != null) 
						{
							try {
								inputStream.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								result=false;
								UserResponseApp.errorLogs.add("Error in closing input stream for file:"+listOfFiles[i].getAbsolutePath()+".Exception:"+e);
								  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
							}
						}
						if (sc != null)
						{
								sc.close();
						}
						
					
				}
				//validate filename before returning operationCompletion value
				validateFile(listOfFiles[i].getName(),lineCounter);
				

				
		  }
		
		return result;
	
	
	}
	
	
	public UserObjectWrapper processRecord(String line)
	{
		String[] cleanRecord=cleanRecord(line);
		UserDet userDet=new UserDet();
		UserProfile userProfile=new UserProfile();
		UserObjectWrapper userObjectWrapper=new UserObjectWrapper();
		userDet.setIsActive((byte) 1);
		userDet.setIsDeleted((byte) 0);
		userDet.setIsPostalMailInterest(null);
		
		userDet.setEnrollmentChanneSource(UserResponseApp.enrollmentChannelSource);
		userDet.setRoles(UserResponseApp.roles);
	//	userProfile.setRoles(UserResponseApp.roles);
		userProfile.setIsPostalMailInterest(new Byte("0"));
	
		//userDet.setBrandedOptCheck((byte) 0);
		userProfile.setIsActive((byte)1);
		userProfile.setIsDeleted((byte)0);
		
		
		
		for(int i=0;i<cleanRecord.length;i++)
		{
			System.out.print(cleanRecord.length+" clean record["+i+"] is "+cleanRecord[i]+", ");
		 try
		 {
			switch (i) {
			case 0:
				userDet.setFirstName(cleanRecord[i].trim());
				userProfile.setFirstName(cleanRecord[i].trim());
				break;

			case 1:
				userDet.setLastName(cleanRecord[i].trim());
				userProfile.setLastName(cleanRecord[i].trim());
				break;
				
			case 2:
				userDet.setEmail(cleanRecord[i].toLowerCase().trim());
				userProfile.setEmail(cleanRecord[i].toLowerCase().trim());
				break;
				
			case 3:
				userDet.setAddress1(cleanRecord[i].trim());
				break;
				
			case 4:
				userDet.setAddress2(cleanRecord[i].trim());
				break;
				
			case 5:
				userDet.setCity(cleanRecord[i].trim());
				break;
				
			case 6:
				userDet.setState(cleanRecord[i].trim());
				break;
				
			case 7:
				userDet.setZip(cleanRecord[i].trim());
				break;
			
			case 8:
				userDet.setContactNumber(cleanRecord[i].trim());
				userProfile.setContactNumber(cleanRecord[i].trim());
				break;
				
			case 9:
				userProfile.setGender(cleanRecord[i]);
				break;
				
			case 10:
				userProfile.setDob(cleanRecord[i]);
				break;
				
			case 11:
				System.out.println("media source id:"+cleanRecord[i]);
				
				userDet.setMediaSourceId(StringUtils.isNotBlank(cleanRecord[i]) ? Integer.parseInt(cleanRecord[i]):null);
				break;
				
			case 12:
				if(cleanRecord[i]==null)
				{
					cleanRecord[i]="";
				}
				userDet.setUuid(cleanRecord[i].trim());
				userProfile.setUserUid(cleanRecord[i].trim());
			System.out.println("UUID..."+cleanRecord[i]);
				break;
				
			case 13:
				int userType;
				if(cleanRecord[i].trim().toUpperCase().equals("L"))
				{
					userType=0;
					String uuid=UUIDGenerator.getUUID();
					userDet.setUuid(uuid);
					userProfile.setUserUid(uuid);
					userDet.setIsCrmEnrolled(0);
				
						}
				else if(cleanRecord[i].trim().toUpperCase().equals("N"))
				{
					userType=1;
					String uuid=UUIDGenerator.getUUID();
					userDet.setUuid(uuid);
					userProfile.setUserUid(uuid);
					userDet.setIsCrmEnrolled(0);
					
				}
				else if(cleanRecord[i].trim().toUpperCase().equals("U"))
				{
					userType=2;
					//user will copy isCRM enrolled from retrieved user obj
					
				}
				else
				{
					userType=3;
					System.out.println("invalid type of record");
					UserResponseApp.errorLogs.add("Invalid user type found:"+cleanRecord[i]);
				}
					userDet.setUserType(userType);
				break;
		
			case 14:
				//CAYQ1
				String relCode=cleanRecord[i].trim().toUpperCase();
				String rel=relCode;
				if(relCode.equalsIgnoreCase("CAYQ1A1"))
				{
					rel="P";
				}
				else if(relCode.equalsIgnoreCase("CAYQ1A2"))
				{
					rel="C";
				}
				userProfile.setRelationship(rel);
				break;
				
			case 15:
				//CAYQ2
				userDet.setCapPatientId(cleanRecord[i].trim());
				break;
				
			case 16:
				//CAYQ3
				userDet.setCapEnrollmentDate(cleanRecord[i].trim());
				break;
				
			case 17:
				//CAYQ4
				userProfile.setSpFirstShipDate(cleanRecord[i].trim());
				break;
			case 18:
				//CAYQ5
				userProfile.setSpMostRecentShipDate(cleanRecord[i].trim());
				String lastShipment=cleanRecord[i].trim();
				if(cleanRecord[i]!=null && !cleanRecord[i].equals(""))
				{
				 lastShipment=DateTime.changeFormat(cleanRecord[i].trim(),"yyyyMMdd","yyyy-MM-dd");
				}
				userProfile.setLastShipment(lastShipment);
				break;
			case 19:
				//CAYQ6
				userProfile.setCapRefillPattern(cleanRecord[i].trim());
				
				/*if(userProfile.getSpFirstShipDate()!=null && userProfile.getSpMostRecentShipDate()==null && (userProfile.getSpFirstShipDate().equals(userProfile.getSpMostRecentShipDate())))
				{
					if(userProfile.getCapRefillPattern()!=null)
					{
					String refillPattern[]=userProfile.getCapRefillPattern().split("\\|");
					int matchCtr=0;
					for(int j=0;i<refillPattern.length;i++)
					{
						if(refillPattern[j].equals(userProfile.getSpFirstShipDate()) || (refillPattern[j].equals(userProfile.getSpMostRecentShipDate())))
								matchCtr++;
					}
					if(matchCtr==refillPattern.length)
					{
						userProfile.setCapRefillPattern("".trim());
					}
					}
				}*/
				
				
				break;
				
			case 20:
				//CAYQ7
				userDet.setPharmacy(cleanRecord[i].trim());
				String pharmacyId=cleanRecord[i].trim();
				String pharmacyName="";
				String pharmacyContactNumber="";
			    Integer pharmacy_id = null;//changed - 1 august 2016 from 0 to null
			    ResourceBundle pharmacyResource=ResourceBundle.getBundle(UserResponseApp.pharmacyInfo);
				if(pharmacyId.equals("CAYQ7A1"))
				{
					pharmacyName=pharmacyResource.getString("pharmacyName1");	
					pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber1");
					pharmacy_id=1;
				}
				else if(pharmacyId.equals("CAYQ7A2"))
				{
					pharmacyName=pharmacyResource.getString("pharmacyName2");
					pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber2");
					pharmacy_id=2;
				}
				else if(pharmacyId.equals("CAYQ7A3"))
				{
					pharmacyName=pharmacyResource.getString("pharmacyName3");
					pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber3");
					pharmacy_id=3;
				}
				else if(pharmacyId.equals("CAYQ7A4"))
				{
					pharmacyName=pharmacyResource.getString("pharmacyName4");
					pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber4");
					pharmacy_id=4;
				}
				
				else if(pharmacyId.equals("CAYQ7A5"))
				{
					pharmacyName=pharmacyResource.getString("pharmacyName5");
					pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber5");
					pharmacy_id=5;
					
				}
				else if(pharmacyId.equals("CAYQ7A6"))
				{
					pharmacyName=pharmacyResource.getString("pharmacyName6");
					pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber6");
					pharmacy_id=6;
					if(pharmacyName==null)
					{
						pharmacyName="".trim();
						pharmacy_id=null;
					}
					if(pharmacyContactNumber==null)
					{
						pharmacy_id=null;
						pharmacyContactNumber="".trim();
					}
					
				}
				else
				{	//error OR EMPTY
					
			
				}				
				userDet.setPharmacyName(pharmacyName.trim());
				userDet.setPharmacyContactNumber(pharmacyContactNumber);
				userDet.setPharmacy_id(pharmacy_id);
				userProfile.setPharmacy(pharmacy_id);
				
				break;
			case 21:
				//CAYQ8
				userDet.setPapPatientStatus(cleanRecord[i].trim());
					break;
			case 22:
				//CAYQ9
				userProfile.setPapRxFirstDispenseDate(cleanRecord[i].trim());
				break;
			case 23:
				//CAYQ10
				userProfile.setPapRxDispenseActivity(cleanRecord[i].trim());
				break;
			case 24:
				//CAYQ11
				userProfile.setPapRxLatestDispenseDate(cleanRecord[i].trim());
				break;
			case 25:
				//CAYQ12
				userDet.setCapHasInsurance(cleanRecord[i].trim());
				break;
			case 26:
				//CAYQ13
				userDet.setCapPatientStatus(cleanRecord[i].trim());
				break;
			case 27:
				//CAYQ14
				userDet.setCapFormId(cleanRecord[i].trim());
				break;
			case 28:
				//CAYQ15
				if(cleanRecord[i].trim().equals("CAYQ15A1"))
				{
					userDet.setBrandedOptCheck(new Byte("1"));
				}
				else if(cleanRecord[i].trim().equals("CAYQ15A2"))
				{
					userDet.setBrandedOptCheck(new Byte("0"));
					userProfile.setNotifyPref(-1);
					userProfile.setRefillReminderNotifyPref(-1);
					userProfile.setDosingReminderNotifyPref(-1);
				}
				else
				{
					System.out.println("invalid cayq15 value in process files");
					//error
				}
				break;
			case 29:
				//CAYQ16
				userDet.setRemainingRefillCount(Integer.parseInt(cleanRecord[i].trim()));
				break;
			case 30:
				//CAYQ17
				userDet.setOptInDate(cleanRecord[i].trim());
				break;
			case 31:
				//CAY18
				userDet.setOptOutDate(cleanRecord[i].trim());
				break;
				
			case 32:
				//CAY19
				userProfile.setPrescriberId(cleanRecord[i].trim());
				break;
			case 33:
				//CAY20
				userProfile.setShipmentStatus(cleanRecord[i].trim());
				break;
			case 34:
				//CAY21
				userProfile.setCfCenter(cleanRecord[i].trim());
				break;
			case 35:
				//CAY22
				userProfile.setCoPayAmount(Integer.parseInt(cleanRecord[i].trim()));
				break;
			
			case 36:
				//CAY23
				userProfile.setAlteraFirstShipmentDate(cleanRecord[i].trim());
				break;
			
			case 37:
				//CAY24
				userProfile.setAlteraUniqueId(cleanRecord[i].trim());
				break;
			default:
				System.out.println("Wrong case no.");
				break;
				
				
				
			}//switch ends
		 }//try ends
			
			catch(Exception e)
			{
				System.out.println("Exception in setting field values for user object. Exception:"+e);
				e.printStackTrace();
				UserResponseApp.errorLogs.add("Exception in setting field values for user object while extracting from record. Exception:"+e);
				  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
			}
		}
		userObjectWrapper.setUserDet(userDet);
		userObjectWrapper.setUserProfile(userProfile);
		return userObjectWrapper;
	}
	
	public String[] cleanRecord(String line)
	{
	
		String[] splitRecord=null;
		String responses[]=null;
		String combined[]=null;
		try
		{
			//split on "," . This avoids split in address fields which may have a comma
		splitRecord=line.split("\" *, *\"");
	 	for(int k=0;k<splitRecord.length;k++)
			{	
					//remove quotes from each field
					splitRecord[k]= (splitRecord[k].replace("\"", "")).trim();
			}
	 	//for question answers
	 	String qrList[]=splitRecord[splitRecord.length-1].replaceAll("~~~","~~").trim().split("~~");
	 	responses=new String[qrList.length];
	 	for(int j=0;j<qrList.length;j++)
	 	{
	 		String qr[]=qrList[j].split("~");
	 		if(qr.length==2)
	 		{
	 		responses[j]=qr[1];	
	 		}
	 		else if(qr.length==1)
	 		{
	 			System.out.println("Empty response for "+qr[0]);
	 			responses[j]="".trim();
	 			}
	 		else
	 		{
	 			System.out.println("error in qr split");
	 		}
	 			
	 	}
	 	combined=(String[])ArrayUtils.addAll(Arrays.copyOf(splitRecord, splitRecord.length-1), responses);
		
		}
		catch(Exception e)
		{
			System.out.println("Exception in creating a clean record from line in file."+e);
			UserResponseApp.errorLogs.add("Exception in creating a clean record from line in file."+e);
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
		}
		finally{
	 	 	splitRecord=null;
	 	return combined;
		}
	}	
	
	
	public boolean validateFile(String name,int count)
	{
		boolean isValid=false;
		if(!name.contains("ARCHES_GILEAD_CAYSTON_CAP_ENROLLMENT_"))
		{
			isValid=false;
		}
		
		String countInName=name.substring(name.lastIndexOf("_")+1,name.lastIndexOf(".txt.pgp"));
		if(countInName.equals(Integer.toString(count)))
				{
					isValid=true;
				}
		if(isValid==false)
		{
			UserResponseApp.errorLogs.add("The record count in the name("+countInName+") and the file ("+count+")do not match for "+name);
			System.out.println("The record count in the name("+countInName+") and the file ("+count+")do not match for "+name);
		}
		System.out.println("isValid for filename:"+name+":"+isValid);
		return isValid;	
	}
	
}
