package com.arches.caystonETL;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

import com.arches.model.UserDet;
import com.arches.utilities.DateTime;
import com.google.gson.Gson;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.controller.MandrillTemplatesApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import com.microtripit.mandrillapp.lutung.view.MandrillTemplate;
import com.microtripit.mandrillapp.lutung.view.MandrillUserInfo;

public class EmailGenerator
{
    private final String mandrillFile;
    private final String senderEmail;
    private final String apiKey;
    private final String senderName;
    private final String[] recipientsEmails; 
    private final String[] recipientsNames;
    private String welcomeEmailOn;
    private String summaryEmailOn;
    private final String  templateName;
    private final String baseURLWelcomeEmail;
    private String REPROFILEURL=null;
    private String SIGNINURL=null;
    private String UNSUBSCRIBEURL=null;
    private String reprofileCode;
    private String reprofileAdditionalString;
    private String unsubscribeAdditionalString;
    
//constructor
    public EmailGenerator(String mandrillFile)
    {
            this.mandrillFile=mandrillFile; //initialize mandrillFile location
            ResourceBundle mandrillResource = ResourceBundle.getBundle(mandrillFile);
            this.senderEmail=mandrillResource.getString("senderEmail");
            this.senderName=mandrillResource.getString("senderName");
            this.apiKey=mandrillResource.getString("apiKey");
            this.recipientsEmails=mandrillResource.getString("recipientsEmails").split(",");
            this.recipientsNames=mandrillResource.getString("recipientsNames").split(",");
            this.welcomeEmailOn=mandrillResource.getString("welcomeEmailOn");
            this.templateName=mandrillResource.getString("templateName");
            this.baseURLWelcomeEmail=mandrillResource.getString("baseURLWelcomeEmail");
            this.summaryEmailOn=mandrillResource.getString("summaryEmailOn");
            this.reprofileCode=mandrillResource.getString("reprofileCode");
            this.reprofileAdditionalString="&utm_source=Enrollment_Form&utm_medium=email&utm_campaign=Welcome_Email";
            this.unsubscribeAdditionalString="&utm_source=Unsubscribe&utm_medium=email&utm_campaign=Welcome_Email";
    }

    
    //method to generate summary email
    public void generateEmail(long total,long successfulRegistration,long successfulReprofiling, long validationErrors, long duplicateEnrollmentError,long UUIDErrorForReprofiling,long reprofilingError,long registrationError,long capIDError) throws MandrillApiError, IOException
    {
    	if(summaryEmailOn==null || summaryEmailOn.equals("false"))
    	{
    		return;
    	}
    	long totalSuccessful=(successfulRegistration+successfulReprofiling);
         //call method to generate email
     
        //MandrillConfiguration gets the mandrill account properties
        MandrillApi mandrillApi=new MandrillApi(this.apiKey);
        MandrillUserInfo user = mandrillApi.users().info();
     
        System.out.println( new Gson().toJson(user) );

       //build email message and add sender information
        MandrillMessage message=new MandrillMessage();
        message.setSubject("Users File Scan Summary For:"+Calendar.getInstance().getTime());
        String styleTemplate="<head><style>"
        		+"h4{color:151C5C;"
        		+ "font-family:Tohoma;}" 
        		+ "#entries {font-family: Tohoma;border-collapse: collapse;width: 100%;font-size:80%;}"
        +"#entries td, #entries th {"
         +"border: 1px solid #ddd;"
         +"text-align: left;"
         +"padding: 8px;}"
         +"#entries tr:nth-child(even){background-color: #f2f2f2}"
         +"#entries tr:hover {background-color: #ddd;}"
         +"#entries th {"
         +"padding-top: 12px;"
         +"padding-bottom: 12px;"
         +"background-color: #0D197C;"
         +"color: white;"
         + "font-weight:bold}"
         +"</style></head>";
        message.setHtml(styleTemplate+"<body><h4 align='center'>Files scan summary report for file(s): "+UserResponseApp.stringOfFileNamesForSummary+"<br>"
        		+Calendar.getInstance().getTime()+"</h4>"
         +"<p><table id=\"entries\" border=\"1px\"><tr><b><th>Title</th><th>No. of Users</th></b></tr>"
        		+ "<tr><td>Total User Records scanned</td><td>"+total+"</td></tr><br>"
        		+ "<tr><td>Number of users successfully registered or reprofiled</td><td>"+totalSuccessful+"</td></tr><br>"
        		+ "<tr><td>Number of users successfully registered </td><td>"+successfulRegistration+"</td></tr><br>"
        		+ "<tr><td>Number of users successfully reprofiled</td><td>"+successfulReprofiling+"</td></tr><br>"
        		+ "<tr><td>Number of users with validation errors</td><td>"+validationErrors+"</td></tr><br>"
        		+ "<tr><td>Number of users with duplicate enrollment error</td><td>"+duplicateEnrollmentError+"</td></tr><br>"
        		+ "<tr><td>Number of users with invalid UUID for reprofiling</td><td>"+UUIDErrorForReprofiling+"</td></tr><br>"
        		+ "<tr><td>Number of users with error in registration</td><td>"+registrationError+"</td></tr><br>"
        		+ "<tr><td>Number of users with error in reprofiling</td><td>"+reprofilingError+"</td></tr>"
        		+ "<tr><td>Number of users with CAP ID error in reprofiling</td><td>"+capIDError+"</td></tr></table><br>"	        		
        		+ "</p></body>");
       
         message.setAutoText(true);
        message.setFromEmail(this.senderEmail);
        message.setFromName(this.senderName);

         List<Recipient> recipients = new ArrayList<Recipient>();  
        for(int i=0;i<recipientsEmails.length;i++)
        {       
        	Recipient recipient = new Recipient();
        	recipient.setEmail(recipientsEmails[i]);
        	recipient.setName(recipientsNames[i]);
        		recipients.add(recipient);
        }
        message.setTo(recipients);
        message.setPreserveRecipients(true);
        try {
      //      send the email
      
          MandrillMessageStatus[] messageStatusReports = mandrillApi.messages().send(message, false);
          for(int j=0;j<messageStatusReports.length;j++)
         {
            System.out.println("Email Sent Status:"+messageStatusReports[j].getStatus());
          } 
        } catch (MandrillApiError ex) {
        System.out.println("mandrill exception."+ex);
        UserResponseApp.errorLogs.add("Exception in generating summary email. Exception:"+ex);
        UserResponseApp.caystonETLLogger.log(Level.SEVERE,ex.getMessage(),ex);
        }
    }
    
    public void generateWelcomeEmail(UserDet user)
    {
    	if(welcomeEmailOn==null || welcomeEmailOn.equals("false"))
    	{
    		return;
    	}
    	
    	this.REPROFILEURL=this.baseURLWelcomeEmail+"enrollment?token="+user.getUuid()+"&c="+this.reprofileCode+this.reprofileAdditionalString;
    	this.SIGNINURL=this.baseURLWelcomeEmail+"signin";
    	this.UNSUBSCRIBEURL=this.baseURLWelcomeEmail+"mailOptoutReq?token="+user.getUuid()+this.unsubscribeAdditionalString;
    	
    	
    	 MandrillApi mandrillApi=new MandrillApi(this.apiKey);
    	 MandrillTemplatesApi mandrilltemplatesapi = mandrillApi.templates();               
         MandrillTemplate MandrillTemplate = null;
		try {
			MandrillTemplate = mandrilltemplatesapi.info(this.templateName);
		} catch (MandrillApiError e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			UserResponseApp.errorLogs.add("Mandrill Api error:"+e1+" in generating welcome email for user:"+new Gson().toJson(user));
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e1.getMessage(),e1);
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			UserResponseApp.errorLogs.add("IO exception:"+e1+" in generating welcome email for user:"+new Gson().toJson(user));
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e1.getMessage(),e1);
			
		}
        String emailContent = MandrillTemplate.getPublishCode();
         
         // create your message
              MandrillMessage message = new MandrillMessage();
              message.setTrackClicks(Boolean.TRUE);
              message.setTrackOpens(Boolean.TRUE);
              
              List<MergeVar> globalMergeVars = new ArrayList<MergeVar>();
              MergeVar objVar = new MergeVar();
              objVar.setName("FNAME");
              objVar.setContent(user.getFirstName());      
              
              MergeVar objVar1 = new MergeVar();
              objVar1.setName("UUID");
              objVar1.setContent(user.getUuid());
              
              MergeVar objVar2 = new MergeVar();
              objVar2.setName("REPROFILEURL");
              objVar2.setContent(REPROFILEURL);
              
              MergeVar objVar3 = new MergeVar();
              objVar3.setName("SIGNINURL");
              objVar3.setContent(SIGNINURL);
              
              MergeVar objVar4 = new MergeVar();
              objVar4.setName("UNSUBSCRIBEURL");
              objVar4.setContent(UNSUBSCRIBEURL);
              
              globalMergeVars.add(objVar);
              globalMergeVars.add(objVar1);
              globalMergeVars.add(objVar2);
              globalMergeVars.add(objVar3);
              globalMergeVars.add(objVar4);
              
              
              message.setGlobalMergeVars(globalMergeVars);
             
             
    	  
    	  
          message.setAutoText(true);
          message.setFromEmail(MandrillTemplate.getFromEmail());
          message.setFromName(MandrillTemplate.getFromName());
          

           List<Recipient> recipients = new ArrayList<Recipient>();  
          	Recipient recipient = new Recipient();
          	recipient.setEmail(user.getEmail());
          	recipient.setName(user.getFirstName());
          	recipients.add(recipient);
          
          message.setTo(recipients);
          message.setPreserveRecipients(true);
          try {
        //      send the email
        	  java.util.Date date = new java.util.Date();
              String sendAt= new SimpleDateFormat("yyyy-MM-dd").format(date);  String hhmmss = getCurrentTimeinHHMMSS();
              
              sendAt = sendAt + " " + hhmmss;
              java.util.Date sendDate = getSendDate(sendAt);
              MandrillMessageStatus[] messageStatusReports = mandrillApi.messages().sendTemplate(templateName, null, message, Boolean.TRUE,"",sendDate);
        
        	  System.out.println("~~~~~~~sending welcome email to:"+user.getEmail());
            for(int j=0;j<messageStatusReports.length;j++)
           {
              System.out.println("Email Sent Status:"+messageStatusReports[j].getStatus());
            } 
          } catch (MandrillApiError ex) {
          System.out.println("mandrill exception."+ex);
          UserResponseApp.errorLogs.add("Mandrill Exception in generating welcome email. Exception:"+ex);
          UserResponseApp.caystonETLLogger.log(Level.SEVERE,ex.getMessage(),ex);
          } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			  UserResponseApp.errorLogs.add("IO Exception in generating welcome email. Exception:"+e);
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
		        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
		}

         
    }
    public java.util.Date getSendDate(String send_at)throws Exception{
        
        String string = send_at;
         DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         java.util.Date sendDate = format.parse(string);
         return sendDate;
    }
      
      public String getCurrentTimeinHHMMSS()
      {
         SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss");//dd/MM/yyyy
         java.util.Date now = new java.util.Date();
         String strDate = sdfDate.format(now);
         System.out.println(strDate);
         
         return strDate;
      
      }
    
}