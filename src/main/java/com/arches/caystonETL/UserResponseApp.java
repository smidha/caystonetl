package com.arches.caystonETL;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.security.auth.login.Configuration;

import org.codehaus.jackson.JsonGenerationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.web.util.Log4jConfigListener;
import com.arches.pgp.PGPTool;
import com.arches.utilities.DateTime;
import com.arches.utilities.ErrorLogger;
import com.arches.utilities.SummaryReportGenerator;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Hello world!
 *
 */
//MAIN class for Processing files received from caremetx and registering users
public class UserResponseApp 
{ 
	
	private static String receivedFilesLocation;
	private static String decryptedFilesLocation;
	private static String pathToPGPArchives;
	private static String pathToTXTArchives;
	private static String pathToEncryptedFiles;
	private static String pathToDecryptedFiles;
	public static List<String> errorLogs;
	public static String configPropertiesLocation;
	public static String mandrillFile;
	public static String enrollmentChannelSource; //DEFAULT
	public static String roles;
	public static String hibernateConfig;
	public static String pharmacyInfo;
	public static Logger caystonETLLogger;
	public static String caystonETLLogLocation;
	public static SessionFactory sessionFactory;
	public static Session session;
	
	public static org.hibernate.cfg.Configuration configuration;
	

	
	//names set in main during decryption
	public static String stringOfFileNamesForSummary="";
	public static long recordsCountsForSummary=0;
	private static void setFilesLocation()
	{//get file folder location for received files and folder location where decrypted files will be stored from properties file
		System.out.println("error logs list initialized..."+errorLogs);
		errorLogs=new ArrayList<String>();
		configPropertiesLocation="config";
		
		ResourceBundle resource = ResourceBundle.getBundle(configPropertiesLocation);
		
		receivedFilesLocation=resource.getString("receivedFilesLocation").trim();
		decryptedFilesLocation=resource.getString("decryptedFilesLocation").trim();
	      pathToPGPArchives=resource.getString("pathToPGPArchives").trim();
	      pathToTXTArchives=resource.getString("pathToTXTArchives").trim();
	      pathToDecryptedFiles=resource.getString("decryptedFilesLocation").trim();
	      pathToEncryptedFiles=resource.getString("receivedFilesLocation").trim();
	      hibernateConfig=resource.getString("hibernateConfig").trim();
	      mandrillFile=resource.getString("mandrillFile").trim();
	      enrollmentChannelSource=resource.getString("enrollmentChannelSource").trim();
	      roles=resource.getString("roles").trim();
	      pharmacyInfo=resource.getString("pharmacyInfo").trim();
	      caystonETLLogLocation=resource.getString("caystonETLLogLocation").trim();
	     
	      caystonETLLogger=Logger.getLogger("caystonETLLogger");
	      configuration=new org.hibernate.cfg.Configuration();
			configuration.configure(hibernateConfig);
			sessionFactory=configuration.buildSessionFactory();
			 session=sessionFactory.openSession();
		
	     
	  
	      
	}
    public static void main(String[] args) throws JsonGenerationException, IOException, ParseException
    {
    
    try{
    	
    		//initialize file location variables
    		setFilesLocation();
    		
    		//prepare the java.util.logger to record any excpetion
    		FileHandler fileHandler;
    		try {
    	  		fileHandler=new FileHandler(UserResponseApp.caystonETLLogLocation+DateTime.getDateTime()+".txt",true);
    	  		caystonETLLogger.addHandler(fileHandler);
    	  		SimpleFormatter simpleFormatter=new SimpleFormatter();
    	  		fileHandler.setFormatter(simpleFormatter);
    	  	   UserResponseApp.caystonETLLogger.log(Level.INFO,"Logging for date:"+DateTime.getDateTime(),"Logging for date:"+DateTime.getDateTime());
    	  	}
    	  	catch(Exception e)
    	  	{
    	  		System.out.println("Exception in configuring logger."+e);
    	  		errorLogs.add("Exception in configuring logger in user response app."+e);
    	  	}
    		
    		//get pgp files for decrypting them
    		FileFilter ff=new FileFilter();
    		//get the pgp files from folder
    		FilenameFilter fileNameFilter=ff.getFilter(".pgp");
    		File dir=new File(receivedFilesLocation);
            File[] listOfFiles = null;
               try
               {
                    //get list of files based on defined filter
                 listOfFiles=dir.listFiles(fileNameFilter);
 		         
                }
               catch(Exception e)
               {
            	   System.out.println("Exception in getting files from:"+receivedFilesLocation);
            	   UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
            	   errorLogs.add("Exception in getting files from:"+receivedFilesLocation);
            	   
               }
      
    	for(int i=0;i<listOfFiles.length;i++)
    	{
    			
    		boolean decryptionResult=false;
    		//use pgptool object for file
    			PGPTool pgpTool=new PGPTool();

    			try {
    				ResourceBundle resource = ResourceBundle.getBundle(UserResponseApp.configPropertiesLocation);

    				//decrypt each file received
					decryptionResult=pgpTool.testDecrypt(listOfFiles[i].getAbsolutePath(),decryptedFilesLocation+"/"+listOfFiles[i].getName()+".txt",resource.getString("PASSPHRASE"),resource.getString("DKI_PRIVATEKEY"));
				} catch (Exception e) {
					
					System.out.println("UserResponseApp:error in decrypting or accessing encrypted file:"+listOfFiles[i].getAbsolutePath()+"Exception:"+e);
					e.printStackTrace();
					  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
					//log error in error logger file
						UserResponseApp.errorLogs.add("Error in decrypting file:"+listOfFiles[i].getName());
					 continue;
				}
    	
    	}
    	//txt files created in folder
    	System.out.println("_______________________Invoking cayston client_______________________");
        
    	//pass the location of decrypted text files to keystone client for registering users
     	ProcessFiles processFiles=new ProcessFiles(decryptedFilesLocation);
 	     boolean operationComplete=processFiles.processRecords();
 	   
    	if(operationComplete==true)
    	{
    		//archive files
			Archiver archiver=new Archiver();
			//archive pgp and txt files
			archiver.archive(pathToEncryptedFiles, ".pgp", pathToPGPArchives);
			archiver.archive(pathToDecryptedFiles,".txt",pathToTXTArchives);
		
    	}
    	else
    	{
    		System.out.println("file processing operation Complete= FALSE. Returned by Invoke Cayston Client.");
    		UserResponseApp.errorLogs.add("file processing operation Complete= FALSE. Returned by Invoke Cayston Client");
    		
    	}
    
    }
    catch(Exception e)
    {
    	System.out.println("Error in UserResponseApp."+e);
    	
    	UserResponseApp.errorLogs.add("Error in User Response App:  Error in processing file. Exception:"+e);
    	  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
    	e.printStackTrace();
    }
    finally
    {
    	
    	SummaryReportGenerator summaryReportGenerator=new SummaryReportGenerator();
    	summaryReportGenerator.generateReport(UserResponseApp.recordsCountsForSummary,CaystonClient.successfulRegistration,CaystonClient.successfulReprofiling,CaystonClient.validationErrorRecords, CaystonClient.duplicateEnrollmentError,CaystonClient.UUIDErrorForReprofiling,CaystonClient.errorInReprofilingRecords,CaystonClient.errorInRegistrationRecords,CaystonClient.CAPIDErrorForReprofiling);
    	
    	EmailGenerator emailGenerator=new EmailGenerator(UserResponseApp.mandrillFile);
    	try {
			emailGenerator.generateEmail(UserResponseApp.recordsCountsForSummary,CaystonClient.successfulRegistration,CaystonClient.successfulReprofiling,CaystonClient.validationErrorRecords, CaystonClient.duplicateEnrollmentError,CaystonClient.UUIDErrorForReprofiling,CaystonClient.errorInReprofilingRecords,CaystonClient.errorInRegistrationRecords,CaystonClient.CAPIDErrorForReprofiling);
		} catch (MandrillApiError e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("Mandrill api error in generating summary email");
			UserResponseApp.errorLogs.add("Error in generating summary email report. "+e1);
		}
    	
    	//create error logs file after generating summary
		ErrorLogger errorLogger=new ErrorLogger();
		try
		{
			//write all errors to file
			System.out.println("-------------writing error logs to file------------");
			errorLogger.writeErrorLogs(UserResponseApp.errorLogs);
			//clear the list of errors
			UserResponseApp.errorLogs.removeAll(UserResponseApp.errorLogs);
			UserResponseApp.errorLogs=null;
		} 
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			System.out.println("Exception:Cannot log the errors in error logger file"+e);
			e.printStackTrace();
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
		}
    	
		for(Handler h:UserResponseApp.caystonETLLogger.getHandlers())
		{
		    h.close();   //must call h.close or a .LCK file will remain.
		}
		if(session!=null)
		{
			session.flush();
			session.clear();
			session.close();
		}
		System.exit(0);
    }
    
    
    
    }

}