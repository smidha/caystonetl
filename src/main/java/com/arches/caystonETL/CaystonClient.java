package com.arches.caystonETL;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.jdbc.Work;

import com.arches.model.RefillHistory;
import com.arches.model.UserDet;
import com.arches.model.UserProfile;
import com.arches.utilities.DateTime;
import com.arches.utilities.UUIDGenerator;
import com.google.gson.Gson;
import com.jcraft.jsch.UserAuthGSSAPIWithMIC;

import edu.emory.mathcs.backport.java.util.Arrays;

public class CaystonClient {

	String hibernateConfig=UserResponseApp.hibernateConfig;
	static long successfulRegistration=0;
	static long successfulReprofiling=0;
	static long validationErrorRecords=0;
	static long duplicateEnrollmentError=0;
	static long UUIDErrorForReprofiling=0;
	static long errorInReprofilingRecords=0;
	static long errorInRegistrationRecords=0;
	static long CAPIDErrorForReprofiling=0;
	
	public CaystonClient()
	{
		
	}
	public String registerUsers(UserDet user, UserProfile userProfile)
	{
		
		System.out.println("in cayston client : register user"+user.getUserType());
		//format dob because DB format us yyyy-MM-dd but coding format and text file format is yyyyMMdd
		if(userProfile.getDob()!=null && userProfile.getDob().length()!=0)
		{
			String formattedDate=DateTime.changeFormat(userProfile.getDob(),"yyyyMMdd","yyyy-MM-dd");
			userProfile.setDob(formattedDate);
			if(formattedDate==null)
			{
			UserResponseApp.errorLogs.add("[CaystonClient.registerUsers]:Error/parse exception in formatting DOB format. DateTime transformation utility returned null");
			}
			System.out.println("Formatted DOB for DB storage:"+userProfile.getDob());
		}
		String response=null;
		
		//update user
		if(user.getUserType()==2)
		{
			//reprofiling code
			response=registerUpdateUser(user, userProfile);
			if(response.equals("error"))
			{
				System.out.println("error response..error in reprofiling");
				
				UserResponseApp.errorLogs.add("'Error' response from reprofiling method for user:"+new Gson().toJson(user));
				errorInReprofilingRecords++;
			
			}
			else if(response.equals("success"))
			{
				System.out.println("successfull reprofiling..");
				successfulReprofiling++;
				
			}
			else if(response.equals("UUIDError"))
			{
				System.out.println("UUID not found:"+user.getUuid());
				UserResponseApp.errorLogs.add("UUID not found in database. Cannot reprofile user:"+new Gson().toJson(user));;
				UUIDErrorForReprofiling++;
			}
			else if(response.equals("CAPIDError"))
			{
				System.out.println("UUID not found:"+user.getUuid());
				UserResponseApp.errorLogs.add("CAP ID not found in database. Cannot reprofile user:"+new Gson().toJson(user));;
				CAPIDErrorForReprofiling++;
			}
			else 
			{
				System.out.println("unknown value returned by reprofiling method");
				UserResponseApp.errorLogs.add("Cannot recognize value/result returned by reprofiling method");
				errorInReprofilingRecords++;
			}
			
		}
		
		//legacy or new user
		else if(user.getUserType().intValue()==0 || user.getUserType().intValue()==1)
		{
			System.out.println("legacy or new: register user");
			 
			BigDecimal bd_createdOn=new BigDecimal(System.currentTimeMillis());
			user.setCreatedOn(bd_createdOn);
			userProfile.setCreatedOn(bd_createdOn);
			//call registration method
			response=registerLegacyOrNewUser(user, userProfile);
			
			//"duplicate","success","error" handled in registerlegacyornewuser itself
			if(response==null)
			{
				System.out.println("null response received from registerLecayOrNewUser for user:"+new Gson().toJson(user));
				UserResponseApp.errorLogs.add("null response received from registerLecayOrNewUser for user:"+new Gson().toJson(user));
				errorInRegistrationRecords++;
			}
		}
		return response;
	}
	public String registerLegacyOrNewUser(UserDet user, UserProfile userProfile)
	{
		System.out.println("in register legacy or new user");
		String result=null;
		
		try{
			//check for duplicate enrollment
			System.out.println("call duplication checking method");
			//call duplication checking method
		    result=checkDuplicateEnrollment(user, userProfile);
		    //analyse response to duplication check
		    
		    //duplicate: do not register
		    if(result.equals("duplicate"))
		    {
		    	duplicateEnrollmentError++;
		    	System.out.println(user.getEmail()+" already exists");
		    	UserResponseApp.errorLogs.add("Duplicate enrollment. User already present in DB. User:"+new Gson().toJson(user));
		    	//immediately return 'duplicate'. Do not proceed ahead with registration
		    	return result;
		    }
			
		    //start registration
			
			/*Configuration cfg=new Configuration();
			cfg.configure(this.pathToHibernateConfig);
			SessionFactory sessionFactory=cfg.buildSessionFactory();
			Session session=sessionFactory.openSession();
		
			/*session.doWork(new Work() {
			
			public void execute(Connection connection) throws SQLException {
				// TODO Auto-generated method stub
				connection.createStatement().execute("SET IDENTITY_INSERT user_det ON");
			}
			});*/
		
		    //Register user in db
			Serializable IdForUser=UserResponseApp.session.save(user);
		
			//DB auto generated ID for userDet is UserId for userProfile
			System.out.println("adding first shipment date for new/legacy user to refill history");
			Serializable refillHistory_firstShipment=UserResponseApp.session.save(refillHistoryFirstShipDateNewLegacyUser(user, userProfile));
					RefillHistory[] refillHistoryPatternArray=refillHistoryRefillPatternNewLegacyUser(user, userProfile);
			System.out.println("adding cap pattern for new/legacy user to refill history");

			if(refillHistoryPatternArray!=null)
			{
				for(int ctr=0;ctr<refillHistoryPatternArray.length;ctr++)
				{
					Serializable refillHistoryPattern=UserResponseApp.session.save(refillHistoryPatternArray[ctr]);
				}
			}	
			System.out.println("adding recent shipment date for new/legacy user to refill history");

			Serializable refillHistory_mostRecent=UserResponseApp.session.save(refillHistoryRecentShipDateNewLegacyUser(user, userProfile));

			//save profile object
			userProfile.setUserId(IdForUser.toString());
			Serializable IdForProfile=UserResponseApp.session.save(userProfile);
			UserResponseApp.session.beginTransaction().commit();
		//	session.flush();
		//	session.clear();
		//	session.close();
		//	session=null;
			System.out.println("Primary key(ID) returned:"+IdForUser.toString());
			result="success";
			successfulRegistration++;
			//new or legacy user registered. send welcome email
			if(user.getEmail()!=null && user.getEmail().length()>0)
			{
			EmailGenerator welcomeEmailGenerator=new EmailGenerator(UserResponseApp.mandrillFile);
			welcomeEmailGenerator.generateWelcomeEmail(user);
			
				
			
			}
			
		}
			
		catch(Exception e)
		{
			UserResponseApp.errorLogs.add("Error in adding user to DB. Exception:"+e);
			e.printStackTrace();
			result="error";
			errorInRegistrationRecords++;
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
		}
		finally
		{
			return result;
		}
		
	}
	
	public String checkDuplicateEnrollment(UserDet user,UserProfile userProfile)
	{
		String result="NotDuplicate";
		//Session session=null;
		try{
		
		//	Configuration cfg=new Configuration();
		//cfg.configure(this.pathToHibernateConfig);
		//SessionFactory sessionFactory=cfg.buildSessionFactory();
		//session=sessionFactory.openSession();
		
			
			String hql_capID = "SELECT capPatientId FROM UserDet where capPatientId=:capIDParam";
			Query query_capID = UserResponseApp.session.createQuery(hql_capID);
			query_capID.setParameter("capIDParam",user.getCapPatientId());
			List resultsbasedOnCapID = query_capID.list();
			
			if(resultsbasedOnCapID!=null && resultsbasedOnCapID.size()!=0)
			{
				//cap id found in db..return 'duplicate' instantly
				result="duplicate";
			
				
			}
			else
			{
				//Empty email,cap id not found.Hence not duplicate 
				result="NotDuplicate";
			
			}
				
			return result;

		}
		
		catch(Exception e)
		{
			System.out.println("Exception in duplication checking.."+e);
			UserResponseApp.errorLogs.add("Exception in duplication checking."+e);
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);			
		}
		finally{
	
			return result;
			}
	}
	
	
	
	
	public String registerUpdateUser(UserDet user,UserProfile userProfile)
	{
		try{
			
			// directly compare using cap patint id since its a constant and mandatory field for each user 
				String resultForNoUUIDCase=registerUpdateUsersWithoutUUID(user,userProfile);
				return resultForNoUUIDCase;
			}			
			
		catch(Exception e)
		{
			System.out.println("Error in reprofiling user."+e);
			UserResponseApp.errorLogs.add("Error in reprofiling method."+e);
			e.printStackTrace();
			  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
			return "error";
		}
	}
	
	public  String registerUpdateUsersWithoutUUID(UserDet user,UserProfile userProfile)
	{	
		System.out.println("registering update user without UUID..");
		String hql = "FROM UserDet where capPatientId=:capIDParam";
		Query query = UserResponseApp.session.createQuery(hql);
		query.setParameter("capIDParam",user.getCapPatientId());
		List results = query.list();
		if(results==null || results.size()==0)
		{
			System.out.println("Cap id not found in DB");
			return "CAPIDError";
		}
		else
		{
			System.out.println("Cap id found in DB..register. list size:"+results.size());
			//reprofile user
			UserDet userDetFromDB=(UserDet) results.get(0);
			
			//update the temp obj from db and send it back to db
			userDetFromDB.setFirstName(user.getFirstName());
			userDetFromDB.setLastName(user.getLastName());
			//update only if the new update record has some data(the user might have updated via site)
			if(user.getEmail()!=null && !user.getEmail().equals(""))
			{
				userDetFromDB.setEmail(user.getEmail());	
			}
			if((user.getAddress1()!=null && user.getZip()!=null && user.getState()!=null) && (!user.getAddress1().equals("") && !user.getZip().equals("") && !user.getState().equals("")))
			{
				userDetFromDB.setAddress1(user.getAddress1());
			
			userDetFromDB.setAddress2(user.getAddress2());
			userDetFromDB.setState(user.getState());
			userDetFromDB.setCity(user.getCity());
			userDetFromDB.setZip(user.getZip());
			}
			if(user.getContactNumber()!=null && !user.getContactNumber().equals(""))
			{
				userDetFromDB.setContactNumber(user.getContactNumber());
			}
		
			
		//uuid stays same
			userDetFromDB.setUserType(user.getUserType());
			//relationship column exists in user profile
			userDetFromDB.setCapPatientId(user.getCapPatientId());
			userDetFromDB.setCapEnrollmentDate(user.getCapEnrollmentDate());
			//first shipment date, recent shipment date,cap refill pattern are in profile
			
			//update pharmacy fron new obj only if it has a non null value
			//because user might have updated his pharmacy via website and text files may still be having null values
			if(user.getPharmacy()!=null && !user.getPharmacy().equals(""))
			{	
				userDetFromDB.setPharmacy(user.getPharmacy());
				
				
				String pharmacyInfoStr=getPharmacyInfo(user.getPharmacy());
				if(pharmacyInfoStr!=null  && pharmacyInfoStr.length()>0)
				{	
				String pharmacyInfo[]=pharmacyInfoStr.split(",");
					if(pharmacyInfo.length==3)
					{	
					userDetFromDB.setPharmacyName(pharmacyInfo[0]);
					userDetFromDB.setPharmacyContactNumber(pharmacyInfo[1]);
					userDetFromDB.setPharmacy_id(Integer.parseInt(pharmacyInfo[2]));
					userProfile.setPharmacy(Integer.parseInt(pharmacyInfo[2]));
					}
				}
			}	
			
			userDetFromDB.setPapPatientStatus(user.getPapPatientStatus());
			
			//pap first dispense date,dispense activity,latest dispense date in profile
			//copy newer values
			userDetFromDB.setCapHasInsurance(user.getCapHasInsurance());
			userDetFromDB.setCapPatientStatus(user.getCapPatientStatus());
			userDetFromDB.setCapFormId(user.getCapFormId());
			userDetFromDB.setBrandedOptCheck(user.getBrandedOptCheck());
			userDetFromDB.setRemainingRefillCount(user.getRemainingRefillCount());
			userDetFromDB.setOptInDate(user.getOptInDate());
			userDetFromDB.setOptOutDate(user.getOptOutDate());
			
			//update id/creation time will already be there in db obj
		
			userProfile.setCreatedOn(userDetFromDB.getCreatedOn());
			//set modified field for user obj since its being modified- user from db might have null or earlier modification date
			
			BigDecimal bd_modifiedOn=new BigDecimal(System.currentTimeMillis());
				
			userDetFromDB.setModifiedOn(bd_modifiedOn);
			userProfile.setModifiedOn(bd_modifiedOn);
			
		
			
			Transaction tx_updateUser=UserResponseApp.session.beginTransaction();
			
			//merge/update user obj
			System.out.println("Sending user obj for MERGING based on cap id");
			UserResponseApp.session.merge(userDetFromDB);
			System.out.println("user merged");
				
			RefillHistory firstShipDate=refillHistoryFirstShipDateUpdateUser(userDetFromDB, userProfile,UserResponseApp.session);
		
		
			List<RefillHistory[]> refillPatternCombinedList=refillHistoryRefillPatternUpdateUser(userDetFromDB, userProfile,UserResponseApp.session);
			

			RefillHistory refillHistoryrecentShipDate=refillHistoryRecentShipDateUpdateUser(userDetFromDB, userProfile,UserResponseApp.session);
			
			
					
			//update shipment date
			if(firstShipDate!=null)
			{
				System.out.println("updating first shipment date for update user in refill history with:"+firstShipDate.getRefilled_on());
				String updateRefillHQL1="update RefillHistory set refilled_on=:refilledOnParam,modifiedOn=:modifiedOnParam where id=:idParam";
				
				Query updateRefillQuery1=UserResponseApp.session.createQuery(updateRefillHQL1);
				
				updateRefillQuery1.setParameter("refilledOnParam",firstShipDate.getRefilled_on());
				updateRefillQuery1.setParameter("idParam",firstShipDate.getId());
				updateRefillQuery1.setParameter("modifiedOnParam",firstShipDate.getModifiedOn());
			
				int result=updateRefillQuery1.executeUpdate();
				System.out.println("result for update user updating first shipment date:"+result);
				
				
							}
		
			
	
			//update refill pattern
			RefillHistory refillPatternOldArray[]=null;
			RefillHistory refillPatternNewArray[]=null;
			if(refillPatternCombinedList!=null)
			{
				refillPatternOldArray=refillPatternCombinedList.get(0);
				refillPatternNewArray=refillPatternCombinedList.get(1);
				
			}
			
			if(refillPatternOldArray!=null)
			{
				for(int ctr=0;ctr<refillPatternOldArray.length;ctr++)
				{
					System.out.println("UPDATING cap pattern for update user in refill history with:"+refillPatternOldArray[ctr].getRefilled_on());
					
				
					if(refillPatternOldArray[ctr]==null)
					{
						continue;
					}
					UserResponseApp.session.merge(refillPatternOldArray[ctr]);
				}
			}
			if(refillPatternNewArray!=null)
			{
				for(int ctr=0;ctr<refillPatternNewArray.length;ctr++)
				{
					System.out.println("SAVING cap pattern for update user in refill history with:"+refillPatternNewArray[ctr].getRefilled_on());
					
				
					if(refillPatternNewArray[ctr]==null)
					{
						continue;
					}
					UserResponseApp.session.save(refillPatternNewArray[ctr]);
				}
			}
			
			
			//update recent ship date
			if(refillHistoryrecentShipDate!=null)
			{
				System.out.println("updating recent shipment date for update user in refill history :"+refillHistoryrecentShipDate.getRefilled_on());
				
				String updateRefillHQL="update RefillHistory set refilled_on=:refilledOnParam,createdOn=:createdOnParam,modifiedOn=:modifiedOnParam where id=:idParam";
				Query updateRefillQuery=UserResponseApp.session.createQuery(updateRefillHQL);
				updateRefillQuery.setParameter("refilledOnParam",refillHistoryrecentShipDate.getRefilled_on());
				updateRefillQuery.setParameter("idParam",refillHistoryrecentShipDate.getId());
				updateRefillQuery.setParameter("createdOnParam",refillHistoryrecentShipDate.getCreatedOn());
				
				updateRefillQuery.setParameter("modifiedOnParam",refillHistoryrecentShipDate.getModifiedOn());
				
				int result2=updateRefillQuery.executeUpdate();
				
			}

			
			String userProfile_hql = "FROM UserProfile where userUid=:userUidFromUserDetDB order by id desc";
			Query userProfile_query = UserResponseApp.session.createQuery(userProfile_hql);
			userProfile_query.setParameter("userUidFromUserDetDB",userDetFromDB.getUuid());
			List userProfile_results = userProfile_query.list();
			UserProfile userProfileFromDB=null;
			if(userProfile_results!=null)
			{
				userProfileFromDB=(UserProfile)userProfile_results.get(0);
			}
			//update userprofilefromdb with received userProfile
			else
			{
				UserResponseApp.errorLogs.add("[CaystonClient:registering update user] user profile table does not results corresponding to thus user's uuid from user_det table");
				System.out.println("no results in userProfile..creating a new obj");
				userProfileFromDB=new UserProfile();
			}
			

			//add a row for userProfile in user profile table
			//user profile from txt file will not be having user_id(PK in user table for this obj).so set it
			userProfile.setUserId(userDetFromDB.getId().toString());
			if(userProfile.getUserUid()!=null && userProfile.getUserUid().length()!=0)
			{
				if(!userProfile.getUserUid().equals(userDetFromDB.getUuid()))
						{
							UserResponseApp.errorLogs.add("The uuid in file and from DB for this CAP ID do not match. Proceeding with usage of uuid from DB and updating data");
							UserResponseApp.caystonETLLogger.log(Level.WARNING,"The uuid in file and from DB for this CAP ID do not match. Proceeding with usage of uuid from DB and updating data", "UUID mismatch");
						}
			}
			
			userProfile.setUserUid(userDetFromDB.getUuid().toString());
			
			
			if(userProfile.getEmail()==null || userProfile.getEmail().equals("")) 
			{
				userProfile.setEmail(userProfileFromDB.getEmail());
			}

			if(userProfile.getContactNumber()==null || userProfile.getContactNumber().equals("")) 
			{
				userProfile.setContactNumber(userProfileFromDB.getContactNumber());
			}
			if(userProfile.getDob()==null || userProfile.getDob().equals(""))
			{
				userProfile.setDob(userProfileFromDB.getDob());
			}
			//the text files might be having null info but user might have provided that and updated it via website
			if(userProfile.getPharmacy()==null || userProfile.getPharmacy().equals(""))
			{
				userProfile.setPharmacy(userProfileFromDB.getPharmacy());
			}
			//handle ADDITIONAL CUSTOM Questions 14/11/16
			if(userProfile.getPrescriberId()==null || ("").equals(userProfile.getPrescriberId()))
			{
				userProfile.setPrescriberId(userProfileFromDB.getPrescriberId());
			}
		
			if(userProfile.getCoPayAmount()==null || ("").equals(userProfile.getCoPayAmount()))
			{
				userProfile.setCoPayAmount(userProfileFromDB.getCoPayAmount());
			}
			
			if(userProfile.getCfCenter()==null || ("").equals(userProfile.getCfCenter()))
			{
				userProfile.setCfCenter(userProfileFromDB.getCfCenter());
			}
			if(userProfile.getShipmentStatus()==null || ("").equals(userProfile.getShipmentStatus()))
			{
				userProfile.setShipmentStatus(userProfileFromDB.getShipmentStatus());
			}
			
			if(userProfile.getAlteraFirstShipmentDate()==null || ("").equals(userProfile.getAlteraFirstShipmentDate()))
			{
				userProfile.setAlteraFirstShipmentDate(userProfileFromDB.getAlteraFirstShipmentDate());
			}
			if(userProfile.getAlteraUniqueId()==null || ("").equals(userProfile.getAlteraUniqueId()))
			{
				userProfile.setAlteraUniqueId(userProfileFromDB.getAlteraUniqueId());
			}
			// end of ADDITIONAL CUSTOM Questions
			
			
			
			//copy values from user profile db obj
			//handle scanrios for notify pref
			//opt in case(cayq15A1)
			if(userProfile.getNotifyPref()==null && userProfileFromDB.getNotifyPref()!=null)
			{
				//rev record shows opt out
				if(userProfileFromDB.getNotifyPref()==-1)
				{
					userProfile.setNotifyPref(1);	
				}
				//revious record shows channel selected
				else if(userProfileFromDB.getNotifyPref()>0)
				{
					userProfile.setNotifyPref(userProfileFromDB.getNotifyPref());
				}
			
			}
			//else if(userProfile.getNotifyPref() is -1(non null), it will stay -1
			//else if userProfile.getNotifyPref() is null but prev record is not null, new record will have with null
			
			
			userProfile.setReasonNotTaking(userProfileFromDB.getReasonNotTaking());
			userProfile.setPrescriptionStatus(userProfileFromDB.getPrescriptionStatus());
			userProfile.setUsageDuration(userProfileFromDB.getUsageDuration());
			userProfile.setRefillReminder(userProfileFromDB.getRefillReminder());
			
			
			if(userProfile.getRefillReminderNotifyPref()==null && userProfileFromDB.getRefillReminderNotifyPref()!=null)
			{
				if(userProfileFromDB.getRefillReminderNotifyPref()==-1)
				{
					userProfile.setRefillReminderNotifyPref(1);
				}
				else if(userProfileFromDB.getRefillReminderNotifyPref()>0)
				{
					userProfile.setRefillReminderNotifyPref(userProfileFromDB.getRefillReminderNotifyPref());
				}
			}
			
	
			
			
			userProfile.setRefillReminderType(userProfileFromDB.getRefillReminderType());
			userProfile.setCourseStartsOn(userProfileFromDB.getCourseStartsOn());
			userProfile.setRefillReminderUpto(userProfileFromDB.getRefillReminderUpto());
			userProfile.setLastRefillReminderOn(userProfileFromDB.getLastRefillReminderOn());
			userProfile.setRefillNotifyStatus(userProfileFromDB.getRefillNotifyStatus());
			userProfile.setDosingReminder(userProfileFromDB.getDosingReminder());
			
			
			
			if(userProfile.getDosingReminderNotifyPref()==null && userProfileFromDB.getDosingReminderNotifyPref()!=null)
			{
				if(userProfileFromDB.getDosingReminderNotifyPref()==-1)
				{
					userProfile.setDosingReminderNotifyPref(1);
				}
				else if(userProfileFromDB.getDosingReminderNotifyPref()>0)
				{
					userProfile.setDosingReminderNotifyPref(userProfileFromDB.getDosingReminderNotifyPref());
				}
			
			}
			
			
			
			userProfile.setDosingReminderUpto(userProfileFromDB.getDosingReminderUpto());
			userProfile.setDoseWeekdayFirst(userProfileFromDB.getDoseWeekdayFirst());
			userProfile.setDoseWeekdaySecond(userProfileFromDB.getDoseWeekdaySecond());
			userProfile.setDoseWeekdayThird(userProfileFromDB.getDoseWeekdayThird());
			userProfile.setDoseWeekendFirst(userProfileFromDB.getDoseWeekendFirst());
			userProfile.setDoseWeekendSecond(userProfileFromDB.getDoseWeekendSecond());
			userProfile.setDoseWeekendThird(userProfileFromDB.getDoseWeekendThird());
			userProfile.setDoseNotifyStatus(userProfileFromDB.getDoseNotifyStatus());
			userProfile.setIsPostalMailInterest(userProfileFromDB.getIsPostalMailInterest());
			userProfile.setSelectedPullArticleUid(userProfileFromDB.getSelectedPullArticleUid());
			userProfile.setSegmentUid(userProfileFromDB.getSegmentUid());
			userProfile.setSegmentName(userProfileFromDB.getSegmentName());
			userProfile.setClassification(userProfileFromDB.getClassification());
			userProfile.setGrouping(userProfileFromDB.getGrouping());
			userProfile.setIsPatient(userProfileFromDB.getIsPatient());
			userProfile.setReadArticleCount(userProfileFromDB.getReadArticleCount());
			userProfile.setUnreadArticleCount(userProfileFromDB.getUnreadArticleCount());
			userProfile.setIsActive(userProfileFromDB.getIsActive());
			userProfile.setIsDeleted(userProfileFromDB.getIsDeleted());
			userProfile.setStatus(userProfileFromDB.getStatus());
			userProfile.setAge(userProfileFromDB.getAge());
			userProfile.setTimezone(userProfileFromDB.getTimezone());
			userProfile.setTimezoneOffset(userProfileFromDB.getTimezoneOffset());
			userProfile.setCopyDoseToWeekends(userProfileFromDB.getCopyDoseToWeekends());
			userProfile.setDoseCourseStartOn(userProfileFromDB.getDoseCourseStartOn());
	
			try
			{
			String updateOldUserProfile="update UserProfile set userUid=:userUidNewParam where userUid=:userUidOldParam";
			Query updateOldUserProfileQuery=UserResponseApp.session.createQuery(updateOldUserProfile);
			updateOldUserProfileQuery.setParameter("userUidNewParam",userProfile.getUserUid()+"_reprofile");
			updateOldUserProfileQuery.setParameter("userUidOldParam",userProfile.getUserUid());
			int updateOldUserProfileResult=updateOldUserProfileQuery.executeUpdate();
			
			}
			catch(Exception e)
			{
				
				UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
				UserResponseApp.errorLogs.add("Exception in updating older user profile record by appending _reprofile."+e.toString());
				errorInReprofilingRecords++;
			}
			Serializable newUserProfileID=UserResponseApp.session.save(userProfile);
			
			
			
			
			tx_updateUser.commit();
			
			System.out.println("session closed. new user profile id:"+newUserProfileID.toString());
			return "success";

		}
		
	}
	public static RefillHistory refillHistoryFirstShipDateNewLegacyUser(UserDet user,UserProfile userProfile)
	{

		RefillHistory firstShipDate=new RefillHistory();
		firstShipDate.setUser_uid(user.getUuid());
	
		if(userProfile.getSpFirstShipDate()!=null && !userProfile.getSpFirstShipDate().equals(""))
		{
			firstShipDate.setRefilled_on(DateTime.changeFormat(userProfile.getSpFirstShipDate(),"yyyyMMdd","yyyy-MM-dd"));
		}
		else
		{
			firstShipDate.setRefilled_on(null);
		}
			firstShipDate.setPharmacy_uid(user.getPharmacy_id());
		firstShipDate.setUuid(UUIDGenerator.getUUID());
		firstShipDate.setIsActive(user.getIsActive());
		firstShipDate.setIsDeleted(user.getIsDeleted());
		BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());
	
		firstShipDate.setCreatedOn(currentTS);
	//	firstShipDate.setModifiedOn(currentTS);
		
		return firstShipDate;

		
	}
	public static RefillHistory refillHistoryRecentShipDateNewLegacyUser(UserDet user,UserProfile userProfile)
	{

		RefillHistory recentShipDate=new RefillHistory();
		recentShipDate.setUser_uid(user.getUuid());
		if(userProfile.getSpMostRecentShipDate()!=null && !(userProfile.getSpMostRecentShipDate().equals("")) && (userProfile.getSpMostRecentShipDate().length()>1))
		{
			recentShipDate.setRefilled_on(DateTime.changeFormat(userProfile.getSpMostRecentShipDate(),"yyyyMMdd","yyyy-MM-dd"));
		}
		else
		{
			recentShipDate.setRefilled_on(null);
		}
		recentShipDate.setPharmacy_uid(user.getPharmacy_id());
		recentShipDate.setUuid(UUIDGenerator.getUUID());
		recentShipDate.setIsActive(user.getIsActive());
		recentShipDate.setIsDeleted(user.getIsDeleted());
		BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());
	
		recentShipDate.setCreatedOn(currentTS);
	//	firstShipDate.setModifiedOn(currentTS);
		return recentShipDate;

		
	}
	
	public static RefillHistory[] refillHistoryRefillPatternNewLegacyUser(UserDet user,UserProfile userProfile)
	{

		String refillPatternString=userProfile.getCapRefillPattern();
		String refillPatternArray[]=null;
		RefillHistory refillHistoryPatternArray[]=null;
		
		
		if(refillPatternString!=null && !refillPatternString.isEmpty() && !refillPatternString.equals(""))
		{
			//split the string of dates
			refillPatternArray=refillPatternString.split("\\|");
			refillPatternArray=sortRefillPatternArray(refillPatternArray);
			refillHistoryPatternArray=new RefillHistory[refillPatternArray.length];
			
			for(int ctr=0;ctr<refillPatternArray.length;ctr++)
			{
				System.out.println("creating refill history object for refill pattern for new/legacy user:");
			
				if(refillPatternArray[ctr]==null || refillPatternArray[ctr].isEmpty())
				{
					RefillHistory refillHistoryinBetweenNull=new RefillHistory();
					refillHistoryinBetweenNull.setUser_uid(user.getUuid());
					refillHistoryinBetweenNull.setPharmacy_uid(user.getPharmacy_id());
					refillHistoryinBetweenNull.setUuid(UUIDGenerator.getUUID());
					refillHistoryinBetweenNull.setIsActive(user.getIsActive());
					refillHistoryinBetweenNull.setIsDeleted(user.getIsDeleted());
					BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());	
					refillHistoryinBetweenNull.setCreatedOn(currentTS);
					refillHistoryinBetweenNull.setRefilled_on(refillPatternArray[ctr]);
					//add the null refilledOn refill history obj  for this ctr value
					//case where yyyymmdd|yyyymmdd||yyyymmdd is found
					//so that hibernate does save a totally null entity
					refillHistoryPatternArray[ctr]=refillHistoryinBetweenNull;
		
					//move to next date in string
					continue;
				}
				//change format of dat
				refillPatternArray[ctr]=DateTime.changeFormat(refillPatternArray[ctr].trim(),"yyyyMMdd","yyyy-MM-dd");
				//create refill history obj with the date
				RefillHistory refillHistory=new RefillHistory();
				refillHistory.setUser_uid(user.getUuid());
				refillHistory.setPharmacy_uid(user.getPharmacy_id());
				refillHistory.setUuid(UUIDGenerator.getUUID());
				refillHistory.setIsActive(user.getIsActive());
				refillHistory.setIsDeleted(user.getIsDeleted());
				BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());	
				refillHistory.setCreatedOn(currentTS);
				refillHistory.setRefilled_on(refillPatternArray[ctr]);
						//add the refill history obj for the date to the array
				refillHistoryPatternArray[ctr]=refillHistory;
				
			}
		}
			else
			{
				//entire refill pattern either empty string or null
				RefillHistory refillHistory=new RefillHistory();
				refillHistory.setUser_uid(user.getUuid());
				refillHistory.setPharmacy_uid(user.getPharmacy_id());
				refillHistory.setUuid(UUIDGenerator.getUUID());
				refillHistory.setIsActive(user.getIsActive());
				refillHistory.setIsDeleted(user.getIsDeleted());
				BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());	
				refillHistory.setCreatedOn(currentTS);
				refillHistory.setRefilled_on(null);
				//just send one null entry
				refillHistoryPatternArray=new RefillHistory[1];
				refillHistoryPatternArray[0]=refillHistory;
				

			}
	
		return refillHistoryPatternArray;
		
	}
	

	public  RefillHistory refillHistoryFirstShipDateUpdateUser(UserDet user,UserProfile userProfile,Session session)
	{// for update with uuid, its receiving userdetfromdb which will have a uuid
		System.out.println("in refillHistoryFirstShipDateUpdateUser. uuid:"+user.getUuid());
		
		//Configuration cfg=new Configuration();
		//cfg.configure(this.pathToHibernateConfig);
		//SessionFactory sessionFactory=cfg.buildSessionFactory();
		//Session session=sessionFactory.openSession();
		//retrieve dates in asc order
		String hql_uuid = " FROM RefillHistory where user_uid=:uuidParam order by created_on";
		Query query_uuid = session.createQuery(hql_uuid);
		//get the oldest value only- first row only
		query_uuid.setMaxResults(1);
		query_uuid.setParameter("uuidParam",user.getUuid());
		List resultsbasedOnuuid = query_uuid.list();
	/*	if(session!=null)
		{//close session for retrieval from refill history table
			session.flush();
			session.clear();
			session.close();
			session=null;
		}
	*/
		RefillHistory firstShipDate=new RefillHistory();
		firstShipDate.setUser_uid(user.getUuid());
		firstShipDate.setPharmacy_uid(user.getPharmacy_id());
		firstShipDate.setUuid(UUIDGenerator.getUUID());
		firstShipDate.setIsActive(user.getIsActive());
		firstShipDate.setIsDeleted(user.getIsDeleted());
		//firstShipDate.setRefilled_on(userProfile.getSpFirstShipDate());
		BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());
	
	//	firstShipDate.setCreatedOn(currentTS);
		firstShipDate.setModifiedOn(currentTS);
	
		
		if(resultsbasedOnuuid!=null && resultsbasedOnuuid.size()>0)
		{
			 RefillHistory refillHistoryFromDB=(RefillHistory)resultsbasedOnuuid.get(0);
			 System.out.println("[update user first ship date] refill table id for user:"+refillHistoryFromDB.getId());
			firstShipDate.setId(refillHistoryFromDB.getId());
			String firstShipDateFromUserProfile=userProfile.getSpFirstShipDate();
			//to format date in userprofile
			if(userProfile.getSpFirstShipDate()!=null && !userProfile.getSpFirstShipDate().equals(""))
			{
			firstShipDateFromUserProfile=DateTime.changeFormat(firstShipDateFromUserProfile,"yyyyMMdd","yyyy-MM-dd");	
			}	
			else if(userProfile.getSpFirstShipDate()==null || userProfile.getSpFirstShipDate().equals(""))
			{
				firstShipDateFromUserProfile=null;
			}
			
			//for case of equal dates in db and text file
				firstShipDate.setRefilled_on(firstShipDateFromUserProfile);
		
			
			
		}
			else
			{
				System.out.println("Cannot find the corresponding uuid in first shipment date matching for refill history.");
				UserResponseApp.errorLogs.add("Cannot find the corresponding uuid for matching first shipment date in refill history for update user without UUID:"+new Gson().toJson(user));
			}
			resultsbasedOnuuid=null;
				return firstShipDate;
		}
	
	public  RefillHistory refillHistoryRecentShipDateUpdateUser(UserDet user,UserProfile userProfile,Session session)
	{
		System.out.println("in refillHistoryRecentDateUpdateUser uuid:"+user.getUuid());
	//	Configuration cfg=new Configuration();
	//	cfg.configure(this.pathToHibernateConfig);
		//SessionFactory sessionFactory=cfg.buildSessionFactory();
		//Session session=sessionFactory.openSession();
		String hql_uuid = " FROM RefillHistory where user_uid=:uuidParam order by created_on desc";
		Query query = session.createQuery(hql_uuid);
		//get the latest value only
		query.setMaxResults(1);
		query.setParameter("uuidParam",user.getUuid());
		System.out.println("retrieving results from refill history table for update user's recent ship date");
		List resultsbasedOnuuid = query.list();
		RefillHistory recentShipDate=new RefillHistory();
		recentShipDate.setUser_uid(user.getUuid());
		recentShipDate.setPharmacy_uid(user.getPharmacy_id());
		recentShipDate.setUuid(UUIDGenerator.getUUID());
		recentShipDate.setIsActive(user.getIsActive());
		recentShipDate.setIsDeleted(user.getIsDeleted());
		BigDecimal currentTS=new BigDecimal(System.currentTimeMillis());
	
		//update its createdon to make sure most recent ship dtae is the latest createdon for that user
		recentShipDate.setCreatedOn(currentTS);
		recentShipDate.setModifiedOn(currentTS);
	


		if(resultsbasedOnuuid!=null & resultsbasedOnuuid.size()>0)
		{
			System.out.println("results from refill history for update user's recent ship date received.Access element 0");
			RefillHistory refillHistoryFromDB=(RefillHistory)resultsbasedOnuuid.get(0);
			//recent ship date is being updated. copy the row id
			recentShipDate.setId(refillHistoryFromDB.getId());
			
			String recentShipDateUserProfile=userProfile.getSpMostRecentShipDate();
			//format recent ship date from user profile obj
			if(recentShipDateUserProfile!=null && !recentShipDateUserProfile.equals(""))
			{
				recentShipDateUserProfile=DateTime.changeFormat(recentShipDateUserProfile,"yyyyMMdd","yyyy-MM-dd");
			}	
			else
			{
				recentShipDateUserProfile=null;
			}
			
			//set the value of recent ship date
				recentShipDate.setRefilled_on(recentShipDateUserProfile);
	
					
		}
			else
			{
				System.out.println("Cannot find the corresponding uuid in most recent shipment date matching for refill history.");
				UserResponseApp.errorLogs.add("Cannot find the corresponding uuid for matching most recent shipment date in refill history for case of  user without uuid:"+new Gson().toJson(user));
			}
	/*	if(session!=null)
		{
			session.flush();
			session.clear();
			session.close();
			session=null;
		}
	*/
		resultsbasedOnuuid=null;
		return recentShipDate;
		}
	
	
	public static List<RefillHistory[]> refillHistoryRefillPatternUpdateUser(UserDet user,UserProfile userProfile, Session session)
	{
		
		System.out.println("refillHistoryPatternupdateuser");
		
		List<RefillHistory[]> combinedList=new ArrayList<RefillHistory[]>();
		/*	Configuration cfg=new Configuration();
		cfg.configure(UserResponseApp.pathToHibernateConfig);
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		Session session=sessionFactory.openSession();*/
		String hql_uuid = " FROM RefillHistory where user_uid=:uuidParam order by created_on";
		Query query_uuid = session.createQuery(hql_uuid);
		query_uuid.setParameter("uuidParam",user.getUuid());
		List resultsbasedOnuuid = query_uuid.list();
		String refillPatternStrArray[]=null;
		RefillHistory refillPatternFromDBArray[]=null;
		RefillHistory[] refillHistoryPatternOldArray=null;
		RefillHistory[] refillHistoryPatternNewArray=null;
		List<RefillHistory> oldUpdatesList=new ArrayList<RefillHistory>();
		
		int k=0;
		//refill pattern array= array from user profile
		String refillPatternString=userProfile.getCapRefillPattern();
		if(refillPatternString!=null && !refillPatternString.equals(""))
		{
			refillPatternStrArray=refillPatternString.split("\\|");
			//sort the received pattern
			refillPatternStrArray=sortRefillPatternArray(refillPatternStrArray);
			
			
		}
		else
		{
			System.out.println("Refill pattern received in file is empty.acceptable case");
			refillPatternStrArray=new String[1];
			// prevents null ptr excpetion
		}
		
		if(resultsbasedOnuuid!=null && resultsbasedOnuuid.size()>2  && refillPatternStrArray!=null)
		{
			refillPatternFromDBArray=new RefillHistory[resultsbasedOnuuid.size()];
			
			//skip first and last record from db because its first ship date  and most recent ship date
			for(k=1;k<resultsbasedOnuuid.size()-1;k++)
			{ 
				int lengthDiff=(resultsbasedOnuuid.size()-2)-refillPatternStrArray.length;
					if(lengthDiff==0 && ((k-1)==refillPatternStrArray.length))
					{
						//for case: where text file and pattern have same no. of elements.
						//additional measures to ensure pattern string odes not give null ptr exc.
						break;
					
					}
					//if k reaches refill pattern length and no. of db records is more than received in pattern and user is not enrolled in crm)
				
					if((k>refillPatternStrArray.length) && (lengthDiff>0) && (user.getIsCrmEnrolled()!=1))
					{
						//for case in which db has more entries of refill pattern than found text file
						//either db got an extra erroneous entry or the text file is missing a refill record
						System.out.println("The refill pattern String in text file has lesser entries than DB's refill pattern string");
						UserResponseApp.errorLogs.add("Logging error & proceeding:The table has more entries for refill pattern than the received String. Either DB has an extra record for user or text file is missing a refill record for:"+new Gson().toJson(user));
						break;
					
					}

					//if user is enrolled in crm, the crm website is adding latest shipment date record itself
					//not an error. just an extra record added by website
					if((k>refillPatternStrArray.length) && (lengthDiff>0) && (user.getIsCrmEnrolled()==1))
					{
						UserResponseApp.caystonETLLogger.log(Level.INFO,"Simple notification (Not an error): user is enrolled in crm, the website is adding latest shipment date record itself also.");
					//not an error. just an extra record added by website");
						break;
					}
					
						//k-1 because db array starts from first ship date. In text file pattern is in between dates
						refillPatternFromDBArray[k]=(RefillHistory)resultsbasedOnuuid.get(k);
						if(refillPatternStrArray[k-1]!=null && !refillPatternStrArray.equals(""))
						{refillPatternStrArray[k-1]=DateTime.changeFormat(refillPatternStrArray[k-1],"yyyyMMdd","yyyy-MM-dd");}
						else
						{
							refillPatternStrArray[k-1]=null;
						}
						//no comparison.SIMPLY UPDATE the correspongind index values(which will be k and k-1 respectively becayse db array has additional first ship date)
						BigDecimal bd_currentTS=new BigDecimal(System.currentTimeMillis());
						
						RefillHistory oldRefillUpdate=new RefillHistory();
						
						oldRefillUpdate.setId(refillPatternFromDBArray[k].getId());
						oldRefillUpdate.setUser_uid(refillPatternFromDBArray[k].getUser_uid());
						oldRefillUpdate.setRefilled_on(refillPatternStrArray[k-1]);//update the value
						oldRefillUpdate.setPharmacy_uid(refillPatternFromDBArray[k].getPharmacy_uid());
						oldRefillUpdate.setPharmacy(refillPatternFromDBArray[k].getPharmacy());
						oldRefillUpdate.setUuid(refillPatternFromDBArray[k].getUuid());
						oldRefillUpdate.setIsActive(refillPatternFromDBArray[k].getIsActive());
						oldRefillUpdate.setIsDeleted(refillPatternFromDBArray[k].getIsDeleted());
						oldRefillUpdate.setStatus(refillPatternFromDBArray[k].getStatus());
						oldRefillUpdate.setCreatedOn(refillPatternFromDBArray[k].getCreatedOn());
						oldRefillUpdate.setCreatedBy(refillPatternFromDBArray[k].getCreatedBy());
						oldRefillUpdate.setModifiedOn(bd_currentTS);
						oldRefillUpdate.setModifiedBy(refillPatternFromDBArray[k].getModifiedBy());
						oldUpdatesList.add(oldRefillUpdate);
				
				
			}
				//comparison with old db values over. 
				refillHistoryPatternOldArray=new RefillHistory[oldUpdatesList.size()];
				refillHistoryPatternOldArray=(RefillHistory[]) oldUpdatesList.toArray(new RefillHistory[oldUpdatesList.size()]);

			
				List<RefillHistory> newEntriesinRefill=new ArrayList<RefillHistory>();
		
				System.out.println("continue counter k for refill pattern string for update user from:"+k);
			
				for(int ctr=k-1;ctr<refillPatternStrArray.length;ctr++)
					{
						if(refillPatternStrArray[ctr]==null || refillPatternStrArray[ctr].isEmpty())
						{
							//refill pattern string is sorted list devoid of nulls
							//this code is additional check
							continue;
						}
						
						RefillHistory refillHistoryNew=new RefillHistory();
				
						//create refill history obj with the date
						refillHistoryNew.setUser_uid(user.getUuid());
						if(refillPatternStrArray[ctr].indexOf("-")>0)
						{
							//already in correct format
							refillHistoryNew.setRefilled_on(refillPatternStrArray[ctr]);
						}
						else
						{
							refillHistoryNew.setRefilled_on(DateTime.changeFormat(refillPatternStrArray[ctr],"yyyyMMdd","yyyy-MM-dd"));
						}
						refillHistoryNew.setPharmacy_uid(user.getPharmacy_id());
						refillHistoryNew.setUuid(UUIDGenerator.getUUID());//new row
						refillHistoryNew.setIsActive(user.getIsActive());
						refillHistoryNew.setIsDeleted(user.getIsDeleted());
						BigDecimal bd_currentTS=new BigDecimal(System.currentTimeMillis());
						
						refillHistoryNew.setCreatedOn(bd_currentTS);
						refillHistoryNew.setModifiedOn(bd_currentTS);
						//no modifiedON since its new row
						//add the refill history obj for the date to the array
							//refillHistoryPatternArray[newEntryCtr]=refillHistory;
						newEntriesinRefill.add(refillHistoryNew);
						System.out.println("new entry for cap refill:"+refillHistoryNew.getRefilled_on());
						
			
					}
			
				refillHistoryPatternNewArray=new RefillHistory[newEntriesinRefill.size()];
				refillHistoryPatternNewArray=(RefillHistory[]) newEntriesinRefill.toArray(new RefillHistory[newEntriesinRefill.size()]);
			}
		else if(resultsbasedOnuuid!=null && resultsbasedOnuuid.size()>2 && (refillPatternStrArray==null || refillPatternStrArray.equals("")))
		{
			//empty refill pattern string was received.
		}
		else if(resultsbasedOnuuid==null)
		{
			System.out.println("[update cap refill pattern for update users]no result from DB using the uuid");
			UserResponseApp.errorLogs.add("[update cap refill pattern for update users]No result from DB table refill history for given uuid for user:"+new Gson().toJson(user));
		}
		else if(resultsbasedOnuuid!=null && resultsbasedOnuuid.size()<3)
		{
			System.out.println("Results from refill history have less than 3 records/rows. is.e one out of first shipment/recent shipment/cap refill is missing");
			UserResponseApp.errorLogs.add("Results from refill history have less than 3 records/rows. is.e one out of first shipment/recent shipment/cap refill is missing for update user:"+new Gson().toJson(user));
		}
			
		else if(resultsbasedOnuuid==null || resultsbasedOnuuid.size()<3)
		{
				System.out.println("[resultsbasedOnuuid!=null && resultsbasedOnuuid.size()>2 returned false in update user's cap refill pattern update. " +new Gson().toJson(user));
				UserResponseApp.errorLogs.add("Either no entries found in refill table or no. of entries for refilled on(null + nonNull) is less than 3 for:" +new Gson().toJson(user));
		}
		/*if(session!=null)
		{
			session.flush();
			session.clear();
			session.close();
			session=null;
		}*/
	
		
		//combine the old and new pattern
		combinedList.add(refillHistoryPatternOldArray);
		combinedList.add(refillHistoryPatternNewArray);
		resultsbasedOnuuid=null;
		return combinedList;
	}
	
	public static String[] sortRefillPatternArray(String[] pattern)
	{
		Date datePattern[]=new Date[pattern.length];
		int countNonNullEntries=0;
		for(int i=0;i<pattern.length;i++)
		{
			try {
				
				if(pattern[i]!=null && !pattern[i].equals(""))
				{
					datePattern[i]=DateTime.StringToDate(pattern[i]);
					countNonNullEntries++;
				}
				}
			catch (ParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("error in converting string to date."+e);
				UserResponseApp.errorLogs.add("[sortRefillPatternArray]error in converting string to date."+e);
				  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
			}
		
		}
		Arrays.sort(datePattern);
		//declare finalized sorted array
		String sortedPattern[]=null;
		if(countNonNullEntries!=0)
		{sortedPattern=new String[countNonNullEntries];}
		else
		{sortedPattern=new String[1];}
		//covert back to string and store in sortedPattern
		int ctrForSortedPattern=0;
		try {
		for(int i=0;i<datePattern.length;i++)
		{
			
				
				if(datePattern[i]!=null && !datePattern[i].equals(""))
				{
					
					sortedPattern[ctrForSortedPattern]=DateTime.DateToString(datePattern[i]);
					ctrForSortedPattern++;	
				}
				else
				{
					//no increment for ctrForSortedPattern
				}
			}	
		}
			catch(Exception e)
			{
				e.printStackTrace();
				UserResponseApp.errorLogs.add("Exception in storing sorted date pattern."+e);
				  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
			}
			finally
			{
				
				return sortedPattern;
			}
		
		
	}
		
	public static String getPharmacyInfo(String pharmacyId)
	{
		if(pharmacyId==null || pharmacyId.length()==0)
		{
			return null;
		}
		ResourceBundle pharmacyResource=ResourceBundle.getBundle(UserResponseApp.pharmacyInfo);
		String pharmacyName="";
		Integer pharmacy_id = null;
		String pharmacyContactNumber="";
		if(pharmacyId.equals("CAYQ7A1"))
		{
			pharmacyName=pharmacyResource.getString("pharmacyName1");
			pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber1");
			pharmacy_id=1;
		}
		else if(pharmacyId.equals("CAYQ7A2"))
		{
			pharmacyName=pharmacyResource.getString("pharmacyName2");
			pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber2");
			pharmacy_id=2;
		}
		else if(pharmacyId.equals("CAYQ7A3"))
		{
			pharmacyName=pharmacyResource.getString("pharmacyName3");
			pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber3");
			pharmacy_id=3;
		}
		else if(pharmacyId.equals("CAYQ7A4"))
		{
			pharmacyName=pharmacyResource.getString("pharmacyName4");
			pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber4");
			pharmacy_id=4;
		}
		
		else if(pharmacyId.equals("CAYQ7A5"))
		{
			pharmacyName=pharmacyResource.getString("pharmacyName5");
			pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber5");
			pharmacy_id=5;
			
		}
		else if(pharmacyId.equals("CAYQ7A6"))
		{
			pharmacyName=pharmacyResource.getString("pharmacyName6");
			pharmacyContactNumber=pharmacyResource.getString("pharmacyContactNumber6");
			pharmacy_id=6;
			
		}
						
			return pharmacyName+","+pharmacyContactNumber+","+Integer.toString(pharmacy_id);
		}
	}

