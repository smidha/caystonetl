package com.arches.caystonETL;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.util.logging.Level;

public class Archiver {
	File[] listOfFiles;
	
    //constructor
    public Archiver()
    {
    	System.out.println("Archiver created..");
     }
    public void archive(String location,String extension,String pathToArchives)
    {
    		System.out.println("Archiving "+extension+ " files");
    		FileFilter ff=new FileFilter();
    		FilenameFilter fileNameFilter=ff.getFilter(extension);
        
    		File dir=new File(location);
              try{
                   //get list of files based on defined filter
               this.listOfFiles=dir.listFiles(fileNameFilter);
		        
                 }
              catch(Exception e)
              {
            	  System.out.println("Exception in Archiver:"+e);	
            	  UserResponseApp.errorLogs.add("Exception[Archiver]: Error in getting list of files from directory.Exception:"+e);
            	  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
              }
              for(int i=0;i<listOfFiles.length;i++)
              {
            	  try{
            		  //move the file to archives
            		  Files.move(listOfFiles[i].toPath(),(new File(pathToArchives+"\\"+listOfFiles[i].getName())).toPath(),java.nio.file.StandardCopyOption.REPLACE_EXISTING);
            	  }
               
            	  
            	  catch(Exception e)
            	  {
            		  UserResponseApp.errorLogs.add("Exception[Archiver]: Error in moving files during archiving.Exception:"+e);
            		  System.out.println("Error in archiving:"+e);
            		  UserResponseApp.caystonETLLogger.log(Level.SEVERE,e.getMessage(),e);
            		  	//error in moving current file
            		  continue;
            	  }
              }
         }
              
        
}
        
    
