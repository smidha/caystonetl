package com.arches.model;

import java.util.List;

public class UserListsWrapper {

	List<UserDet> userDet;
	List<UserProfile> userProfile;
	public List<UserDet> getUserDet() {
		return userDet;
	}
	public void setUserDet(List<UserDet> userDet) {
		this.userDet = userDet;
	}
	public List<UserProfile> getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(List<UserProfile> userProfile) {
		this.userProfile = userProfile;
	}
	
	
}
