package com.arches.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "refill_history", catalog = "stage_cayston")
public class RefillHistory implements java.io.Serializable {

	private BigDecimal id;
	private String user_uid;
    private String refilled_on;
	private Integer pharmacy_uid;
	private String pharmacy;
	private String uuid;
	private Byte isActive;
	private Byte isDeleted;
	private Integer status;
	private BigDecimal createdOn;
	private String createdBy;
	private BigDecimal modifiedOn;
	private String modifiedBy;
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, scale = 0)
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	@Column(name = "user_uid", length = 36)
	public String getUser_uid() {
		return user_uid;
	}
	@Column(name = "uid", length = 36)
	public void setUser_uid(String user_uid) {
		this.user_uid = user_uid;
	}
	@Column(name = "refilled_on")
	public String getRefilled_on() {
		return refilled_on;
	}
	public void setRefilled_on(String refilled_on) {
		this.refilled_on = refilled_on;
	}
	@Column(name = "pharmacy_uid")
	public Integer getPharmacy_uid() {
		return pharmacy_uid;
	}
	public void setPharmacy_uid(Integer pharmacy_uid) {
		this.pharmacy_uid = pharmacy_uid;
	}
	@Column(name = "pharmacy")
	public String getPharmacy() {
		return pharmacy;
	}
	public void setPharmacy(String pharmacy) {
		this.pharmacy = pharmacy;
	}
	@Column(name = "uuid")
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	@Column(name = "is_active")
	public Byte getIsActive() {
		return isActive;
	}
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}
	@Column(name = "is_deleted")
	public Byte getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Column(name = "created_on")
	public BigDecimal getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(BigDecimal createdOn) {
		this.createdOn = createdOn;
	}
	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(name = "modified_on")
	public BigDecimal getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(BigDecimal modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	@Column(name = "modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
	
	
	
	
}
