package com.arches.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Prescriber", catalog = "stage_cayston")
public class Prescriber implements Serializable
{
Integer id;
String prescriberName;
String prescriberId;
String phone1;
String fax;
String email;

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name = "id", unique = true, nullable = false, scale = 0)

public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
@Column(name = "prescriber_name")
public String getPrescriberName() {
	return prescriberName;
}
public void setPrescriberName(String prescriber_name) {
	this.prescriberName = prescriber_name;
}
@Column(name = "prescriber_id")
public String getPrescriberId() {
	return prescriberId;
}
public void setPrescriberId(String prescriber_id) {
	this.prescriberId = prescriber_id;
}
@Column(name = "phone1")
public String getPhone1() {
	return phone1;
}

public void setPhone1(String phone1) {
	this.phone1 = phone1;
}
@Column(name = "fax")
public String getFax() {
	return fax;
}
public void setFax(String fax) {
	this.fax = fax;
}
@Column(name = "email")
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}

}
