package com.caremetx.client;

import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class CareMetxClient {
	private com.sun.jersey.api.client.Client client;
	
	public CareMetxClient()
	{
		this.client=com.sun.jersey.api.client.Client.create();
	}
	public ClientResponse callCareMetx(String username,String password,String firstname,String lastname,String DOB,String zip)
	{		
		JsonObject json=new JsonObject();
		json.addProperty("username",username);
		json.addProperty("password",password);
		json.addProperty("firstname",firstname);
		json.addProperty("lastname",lastname);
		json.addProperty("DOB",DOB);
		json.addProperty("zip",zip);
		WebResource base=client.resource("https://apiservicestest.caremetx.com/Cayston/ARCHESFindPatient");
		
		ClientResponse response = base.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,new Gson().toJson(json));
		return response;
	}
	
	public static void main(String[] args)
	{
		CareMetxClient c=new CareMetxClient();
		ClientResponse response=c.callCareMetx("testuser","ARCHESTest123!","raphelsd","nidalxc"," 04/13/1980","20203");
		System.out.println("RESPONSE status:"+response.getStatus());
		System.out.println("Getting string.class entity from response:"+response.getEntity(String.class));

	}
}
